-- --------------------------------------------------------
-- Hôte :                        127.0.0.1
-- Version du serveur:           8.0.15 - MySQL Community Server - GPL
-- SE du serveur:                Win64
-- HeidiSQL Version:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Listage de la structure de la base pour ticketcenter
DROP DATABASE IF EXISTS `ticketcenter`;
CREATE DATABASE IF NOT EXISTS `ticketcenter` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ticketcenter`;

-- Listage de la structure de la table ticketcenter. categories
DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `idcategory` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`idcategory`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- Les données exportées n'étaient pas sélectionnées.
-- Listage de la structure de la table ticketcenter. priorities
DROP TABLE IF EXISTS `priorities`;
CREATE TABLE IF NOT EXISTS `priorities` (
  `idpriority` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`idpriority`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- Les données exportées n'étaient pas sélectionnées.
-- Listage de la structure de la table ticketcenter. status
DROP TABLE IF EXISTS `status`;
CREATE TABLE IF NOT EXISTS `status` (
  `idstatus` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`idstatus`)
) ENGINE=InnoDB AUTO_INCREMENT=3214 DEFAULT CHARSET=utf8;

-- Les données exportées n'étaient pas sélectionnées.
-- Listage de la structure de la table ticketcenter. technicians
DROP TABLE IF EXISTS `technicians`;
CREATE TABLE IF NOT EXISTS `technicians` (
  `idtechnician` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `password` varchar(255) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idtechnician`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Les données exportées n'étaient pas sélectionnées.
-- Listage de la structure de la table ticketcenter. tickets
DROP TABLE IF EXISTS `tickets`;
CREATE TABLE IF NOT EXISTS `tickets` (
  `idticket` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `categories_idcategory` int(11) NOT NULL,
  `description` longtext NOT NULL,
  `priorities_idpriority` int(11) NOT NULL,
  `datestart` date NOT NULL,
  `dateend` date DEFAULT NULL,
  `status_idstatus` int(11) NOT NULL,
  `users_iduser` int(11) NOT NULL,
  `technicians_idtechnician` int(11) DEFAULT NULL,
  PRIMARY KEY (`idticket`),
  KEY `fk_tickets_users_idx` (`users_iduser`),
  KEY `fk_tickets_technicians1_idx` (`technicians_idtechnician`),
  KEY `fk_tickets_categories1_idx` (`categories_idcategory`),
  KEY `fk_tickets_priorities1_idx` (`priorities_idpriority`),
  KEY `fk_tickets_status1_idx` (`status_idstatus`),
  CONSTRAINT `fk_tickets_categories1` FOREIGN KEY (`categories_idcategory`) REFERENCES `categories` (`idcategory`),
  CONSTRAINT `fk_tickets_priorities1` FOREIGN KEY (`priorities_idpriority`) REFERENCES `priorities` (`idpriority`),
  CONSTRAINT `fk_tickets_status1` FOREIGN KEY (`status_idstatus`) REFERENCES `status` (`idstatus`),
  CONSTRAINT `fk_tickets_technicians1` FOREIGN KEY (`technicians_idtechnician`) REFERENCES `technicians` (`idtechnician`),
  CONSTRAINT `fk_tickets_users` FOREIGN KEY (`users_iduser`) REFERENCES `users` (`iduser`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Les données exportées n'étaient pas sélectionnées.
-- Listage de la structure de la table ticketcenter. users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=utf8;

-- Les données exportées n'étaient pas sélectionnées.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
