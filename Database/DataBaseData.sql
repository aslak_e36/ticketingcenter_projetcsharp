-- --------------------------------------------------------
-- Hôte :                        127.0.0.1
-- Version du serveur:           8.0.15 - MySQL Community Server - GPL
-- SE du serveur:                Win64
-- HeidiSQL Version:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Listage des données de la table ticketcenter.categories : ~10 rows (environ)
DELETE FROM `categories`;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`idcategory`, `name`) VALUES
	(0, 'Audio-Visuel'),
	(1, 'Gestion des comptes et messageries utilisateur'),
	(2, 'Logiciel'),
	(3, 'Serveur'),
	(4, 'Matériel'),
	(5, 'Serveur'),
	(6, 'Réseau'),
	(7, 'Restauration/Sauveguarde'),
	(8, 'Intranet'),
	(9, 'Autres');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Listage des données de la table ticketcenter.priorities : ~5 rows (environ)
DELETE FROM `priorities`;
/*!40000 ALTER TABLE `priorities` DISABLE KEYS */;
INSERT INTO `priorities` (`idpriority`, `name`) VALUES
	(0, 'Très basse'),
	(1, 'Basse'),
	(2, 'Moyenne'),
	(3, 'Urgente'),
	(4, 'Très urgente');
/*!40000 ALTER TABLE `priorities` ENABLE KEYS */;

-- Listage des données de la table ticketcenter.status : ~6 rows (environ)
DELETE FROM `status`;
/*!40000 ALTER TABLE `status` DISABLE KEYS */;
INSERT INTO `status` (`idstatus`, `name`) VALUES
	(0, 'Nouveau'),
	(1, 'En cours'),
	(2, 'En attente'),
	(3, 'Résolu'),
	(4, 'Clos'),
	(5, 'Non-réslu');
/*!40000 ALTER TABLE `status` ENABLE KEYS */;

-- Listage des données de la table ticketcenter.technicians : ~1 rows (environ)
DELETE FROM `technicians`;
/*!40000 ALTER TABLE `technicians` DISABLE KEYS */;
INSERT INTO `technicians` (`idtechnician`, `email`, `password`, `type`) VALUES
	(1, 'admin', 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855', 0),
	(2, 'technician', 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855', 1);
/*!40000 ALTER TABLE `technicians` ENABLE KEYS */;

-- Listage des données de la table ticketcenter.tickets : ~3 rows (environ)
DELETE FROM `tickets`;
/*!40000 ALTER TABLE `tickets` DISABLE KEYS */;
INSERT INTO `tickets` (`idticket`, `title`, `categories_idcategory`, `description`, `priorities_idpriority`, `datestart`, `dateend`, `status_idstatus`, `users_iduser`, `technicians_idtechnician`) VALUES
	(0, 'Ticket #0', 2, 'J\'ai un problème avec mon logiciel photoshop, il ne veut plus s\'ouvrir', 2, '2020-01-12', '2020-01-16', 3, 100, NULL),
	(1, 'Ticket #1', 6, 'Ma connexion internet est extrement lente depuis ce matin', 3, '2020-01-14', NULL, 1, 200, 1),
	(2, 'Ticket #2', 0, 'Je n\'ai plus de son', 1, '2020-01-15', NULL, 2, 200, NULL);
/*!40000 ALTER TABLE `tickets` ENABLE KEYS */;

-- Listage des données de la table ticketcenter.users : ~2 rows (environ)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`iduser`, `email`, `password`) VALUES
	(100, 'arben@ferati.com', 'a4b87bebb14f3556b6a712608923416bea9756cee94bcebc9c523c7eeb01bc9b'),
	(200, 'ricardo@dantas.com', 'a4b87bebb14f3556b6a712608923416bea9756cee94bcebc9c523c7eeb01bc9b');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
