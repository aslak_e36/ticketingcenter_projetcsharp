﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ModelTicketingCenter;

namespace TestTicketingCenter
{
    /// <summary>
    /// Summary description for TestUser
    /// </summary>
    [TestClass]
    public class TestUser
    {

        [TestMethod]
        public void LoginUser_BaseTest_Success()
        {
            //given
            string email = "arben@ferati.com";
            string password = "Pa$$W0rd";
            bool actualResult;
            //then
            actualResult = Login.LoginUser(email, password);
            //when
            Assert.IsTrue(actualResult);

        }

        [TestMethod]
        public void RegisterUser_BaseTest_Success()
        {
            //given
            string email = "ricardo@dantas.com";
            string password = "Pa$$W0rd";
            bool actualResult;
            //then
            actualResult = Register.RegisterUser(email, password);
            //when
            Assert.IsTrue(actualResult);
        }

        [TestMethod]
        [ExpectedException(typeof(ExistingEmailException))]
        public void RegisterUser_ExistingEmail_Exception()
        {
            //given
            string email = "arben@ferati.com";
            string password = "1234";
            //then
            DbConnector connector = new DbConnector();
            Register.RegisterUser(email, password);
        }

        [TestClass]
        public class UserSettingsTest
        {
            [TestMethod]
            public void UserSettings_BaseTest_Success()
            {
            }

            [TestMethod]
            public void AppSettings_BaseTest_Success()
            {
                //given
                AppSettings expectedResult = new AppSettings();
                AppSettings actualResult;
                expectedResult.DbStringConnection = "Server=localhost;Port=3306;Database=ticketcenter;Uid=root;Pwd=toor;";
                //then
                actualResult = JsonReader.ReadDbConnectionSettings();
                //when
                Assert.AreEqual(expectedResult.DbStringConnection, actualResult.DbStringConnection);
            }
        }


    }
}
