﻿namespace ViewTicketingCenter
{
    partial class UCTickets
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCategoryTicket = new System.Windows.Forms.Label();
            this.lblCategorie = new System.Windows.Forms.Label();
            this.lblDescriptionTicket = new System.Windows.Forms.Label();
            this.lblStatusTicket = new System.Windows.Forms.Label();
            this.lblDateEndTicket = new System.Windows.Forms.Label();
            this.lblDateStartTicket = new System.Windows.Forms.Label();
            this.lblTechnicianTicket = new System.Windows.Forms.Label();
            this.lblPriorityTicket = new System.Windows.Forms.Label();
            this.lblUserTicket = new System.Windows.Forms.Label();
            this.lblTitleTicket = new System.Windows.Forms.Label();
            this.lblTechnician = new System.Windows.Forms.Label();
            this.lblUser = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblDateEnd = new System.Windows.Forms.Label();
            this.lblDateStart = new System.Windows.Forms.Label();
            this.lblPriority = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblCategoryTicket
            // 
            this.lblCategoryTicket.AutoSize = true;
            this.lblCategoryTicket.Font = new System.Drawing.Font("Berlin Sans FB", 10.25F);
            this.lblCategoryTicket.Location = new System.Drawing.Point(532, 32);
            this.lblCategoryTicket.Name = "lblCategoryTicket";
            this.lblCategoryTicket.Size = new System.Drawing.Size(17, 16);
            this.lblCategoryTicket.TabIndex = 35;
            this.lblCategoryTicket.Text = "...";
            // 
            // lblCategorie
            // 
            this.lblCategorie.AutoSize = true;
            this.lblCategorie.Font = new System.Drawing.Font("Berlin Sans FB", 10.25F);
            this.lblCategorie.Location = new System.Drawing.Point(462, 32);
            this.lblCategorie.Name = "lblCategorie";
            this.lblCategorie.Size = new System.Drawing.Size(72, 16);
            this.lblCategorie.TabIndex = 34;
            this.lblCategorie.Text = "Categorie :";
            // 
            // lblDescriptionTicket
            // 
            this.lblDescriptionTicket.AutoSize = true;
            this.lblDescriptionTicket.Font = new System.Drawing.Font("Berlin Sans FB", 10.25F);
            this.lblDescriptionTicket.Location = new System.Drawing.Point(16, 72);
            this.lblDescriptionTicket.Name = "lblDescriptionTicket";
            this.lblDescriptionTicket.Size = new System.Drawing.Size(17, 16);
            this.lblDescriptionTicket.TabIndex = 33;
            this.lblDescriptionTicket.Text = "...";
            // 
            // lblStatusTicket
            // 
            this.lblStatusTicket.AutoSize = true;
            this.lblStatusTicket.Font = new System.Drawing.Font("Berlin Sans FB", 10.25F);
            this.lblStatusTicket.Location = new System.Drawing.Point(509, 10);
            this.lblStatusTicket.Name = "lblStatusTicket";
            this.lblStatusTicket.Size = new System.Drawing.Size(17, 16);
            this.lblStatusTicket.TabIndex = 32;
            this.lblStatusTicket.Text = "...";
            // 
            // lblDateEndTicket
            // 
            this.lblDateEndTicket.AutoSize = true;
            this.lblDateEndTicket.Font = new System.Drawing.Font("Berlin Sans FB", 10.25F);
            this.lblDateEndTicket.Location = new System.Drawing.Point(388, 100);
            this.lblDateEndTicket.Name = "lblDateEndTicket";
            this.lblDateEndTicket.Size = new System.Drawing.Size(17, 16);
            this.lblDateEndTicket.TabIndex = 31;
            this.lblDateEndTicket.Text = "...";
            // 
            // lblDateStartTicket
            // 
            this.lblDateStartTicket.AutoSize = true;
            this.lblDateStartTicket.Font = new System.Drawing.Font("Berlin Sans FB", 10.25F);
            this.lblDateStartTicket.Location = new System.Drawing.Point(127, 100);
            this.lblDateStartTicket.Name = "lblDateStartTicket";
            this.lblDateStartTicket.Size = new System.Drawing.Size(17, 16);
            this.lblDateStartTicket.TabIndex = 30;
            this.lblDateStartTicket.Text = "...";
            // 
            // lblTechnicianTicket
            // 
            this.lblTechnicianTicket.AutoSize = true;
            this.lblTechnicianTicket.Font = new System.Drawing.Font("Berlin Sans FB", 10.25F);
            this.lblTechnicianTicket.Location = new System.Drawing.Point(330, 32);
            this.lblTechnicianTicket.Name = "lblTechnicianTicket";
            this.lblTechnicianTicket.Size = new System.Drawing.Size(17, 16);
            this.lblTechnicianTicket.TabIndex = 29;
            this.lblTechnicianTicket.Text = "...";
            // 
            // lblPriorityTicket
            // 
            this.lblPriorityTicket.AutoSize = true;
            this.lblPriorityTicket.Font = new System.Drawing.Font("Berlin Sans FB", 10.25F);
            this.lblPriorityTicket.Location = new System.Drawing.Point(313, 10);
            this.lblPriorityTicket.Name = "lblPriorityTicket";
            this.lblPriorityTicket.Size = new System.Drawing.Size(17, 16);
            this.lblPriorityTicket.TabIndex = 28;
            this.lblPriorityTicket.Text = "...";
            // 
            // lblUserTicket
            // 
            this.lblUserTicket.AutoSize = true;
            this.lblUserTicket.Font = new System.Drawing.Font("Berlin Sans FB", 10.25F);
            this.lblUserTicket.Location = new System.Drawing.Point(87, 32);
            this.lblUserTicket.Name = "lblUserTicket";
            this.lblUserTicket.Size = new System.Drawing.Size(17, 16);
            this.lblUserTicket.TabIndex = 27;
            this.lblUserTicket.Text = "...";
            // 
            // lblTitleTicket
            // 
            this.lblTitleTicket.AutoSize = true;
            this.lblTitleTicket.Font = new System.Drawing.Font("Berlin Sans FB", 10.25F);
            this.lblTitleTicket.Location = new System.Drawing.Point(57, 10);
            this.lblTitleTicket.Name = "lblTitleTicket";
            this.lblTitleTicket.Size = new System.Drawing.Size(17, 16);
            this.lblTitleTicket.TabIndex = 26;
            this.lblTitleTicket.Text = "...";
            // 
            // lblTechnician
            // 
            this.lblTechnician.AutoSize = true;
            this.lblTechnician.Font = new System.Drawing.Font("Berlin Sans FB", 10.25F);
            this.lblTechnician.Location = new System.Drawing.Point(258, 32);
            this.lblTechnician.Name = "lblTechnician";
            this.lblTechnician.Size = new System.Drawing.Size(73, 16);
            this.lblTechnician.TabIndex = 25;
            this.lblTechnician.Text = "Technicien :";
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = true;
            this.lblUser.Font = new System.Drawing.Font("Berlin Sans FB", 10.25F);
            this.lblUser.Location = new System.Drawing.Point(16, 32);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(71, 16);
            this.lblUser.TabIndex = 24;
            this.lblUser.Text = "Utilisateur :";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("Berlin Sans FB", 10.25F);
            this.lblStatus.Location = new System.Drawing.Point(462, 10);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(51, 16);
            this.lblStatus.TabIndex = 23;
            this.lblStatus.Text = "Status :";
            // 
            // lblDateEnd
            // 
            this.lblDateEnd.AutoSize = true;
            this.lblDateEnd.Font = new System.Drawing.Font("Berlin Sans FB", 10.25F);
            this.lblDateEnd.Location = new System.Drawing.Point(309, 100);
            this.lblDateEnd.Name = "lblDateEnd";
            this.lblDateEnd.Size = new System.Drawing.Size(81, 16);
            this.lblDateEnd.TabIndex = 22;
            this.lblDateEnd.Text = "Date de fin :";
            // 
            // lblDateStart
            // 
            this.lblDateStart.AutoSize = true;
            this.lblDateStart.Font = new System.Drawing.Font("Berlin Sans FB", 10.25F);
            this.lblDateStart.Location = new System.Drawing.Point(16, 100);
            this.lblDateStart.Name = "lblDateStart";
            this.lblDateStart.Size = new System.Drawing.Size(115, 16);
            this.lblDateStart.TabIndex = 21;
            this.lblDateStart.Text = "Date d\'ouverture :";
            // 
            // lblPriority
            // 
            this.lblPriority.AutoSize = true;
            this.lblPriority.Font = new System.Drawing.Font("Berlin Sans FB", 10.25F);
            this.lblPriority.Location = new System.Drawing.Point(258, 10);
            this.lblPriority.Name = "lblPriority";
            this.lblPriority.Size = new System.Drawing.Size(57, 16);
            this.lblPriority.TabIndex = 20;
            this.lblPriority.Text = "Priorité :";
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Font = new System.Drawing.Font("Berlin Sans FB", 10.25F);
            this.lblDescription.Location = new System.Drawing.Point(16, 56);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(78, 16);
            this.lblDescription.TabIndex = 19;
            this.lblDescription.Text = "Description :";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Berlin Sans FB", 10.25F);
            this.lblTitle.Location = new System.Drawing.Point(16, 10);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(41, 16);
            this.lblTitle.TabIndex = 18;
            this.lblTitle.Text = "Titre :";
            // 
            // UCTickets
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Salmon;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.lblCategoryTicket);
            this.Controls.Add(this.lblCategorie);
            this.Controls.Add(this.lblDescriptionTicket);
            this.Controls.Add(this.lblStatusTicket);
            this.Controls.Add(this.lblDateEndTicket);
            this.Controls.Add(this.lblDateStartTicket);
            this.Controls.Add(this.lblTechnicianTicket);
            this.Controls.Add(this.lblPriorityTicket);
            this.Controls.Add(this.lblUserTicket);
            this.Controls.Add(this.lblTitleTicket);
            this.Controls.Add(this.lblTechnician);
            this.Controls.Add(this.lblUser);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.lblDateEnd);
            this.Controls.Add(this.lblDateStart);
            this.Controls.Add(this.lblPriority);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.lblTitle);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "UCTickets";
            this.Size = new System.Drawing.Size(641, 125);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblCategoryTicket;
        private System.Windows.Forms.Label lblCategorie;
        private System.Windows.Forms.Label lblDescriptionTicket;
        private System.Windows.Forms.Label lblStatusTicket;
        private System.Windows.Forms.Label lblDateEndTicket;
        private System.Windows.Forms.Label lblDateStartTicket;
        private System.Windows.Forms.Label lblTechnicianTicket;
        private System.Windows.Forms.Label lblPriorityTicket;
        private System.Windows.Forms.Label lblUserTicket;
        private System.Windows.Forms.Label lblTitleTicket;
        private System.Windows.Forms.Label lblTechnician;
        private System.Windows.Forms.Label lblUser;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label lblDateEnd;
        private System.Windows.Forms.Label lblDateStart;
        private System.Windows.Forms.Label lblPriority;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Label lblTitle;
    }
}
