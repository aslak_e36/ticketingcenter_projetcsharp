﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ModelTicketingCenter;

namespace ViewTicketingCenter
{
    public partial class FrmLogin : Form
    {
        private User user = new User();
        Password password = new Password();
        private JsonReader json;

        public FrmLogin()
        {
            InitializeComponent();
        }
        /// <summary>
        /// This function closes the window and stop the program
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        /// <summary>
        /// This function minimize the window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button1_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        /// <summary>
        /// This function underlines the text in the lbInscription when you have your mouse on it
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LblInscription_MouseEnter(object sender, EventArgs e)
        {
            lblInscription.Font = new Font(lblInscription.Font.Name, lblInscription.Font.SizeInPoints, FontStyle.Underline);
        }
        /// <summary>
        /// This function remove underlined text in the lbInscription when you leave your mouse on it
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LblInscription_MouseLeave(object sender, EventArgs e)
        {
            lblInscription.Font = new Font(lblInscription.Font.Name, lblInscription.Font.SizeInPoints, FontStyle.Regular);
        }
        /// <summary>
        /// This function open register's window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LblInscription_Click(object sender, EventArgs e)
        {
            FrmRegister frmRegister = new FrmRegister();
            frmRegister.Show();
            Visible = false;
        }
        /// <summary>
        /// This function checks if all parameters are OK to login
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdLogin_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtEmail.Text != "" && txtPassword.Text != "")
                {
                    if (Login.LoginUser(txtEmail.Text, password.CurrentPassword, this.user))
                    {
                        FrmHome frmHome = new FrmHome(this.user);
                        frmHome.Show();
                        Visible = false;
                        json.Write(lblEmail.Text);
                    }
                }
                else
                {
                    throw new Exception("Veuillez remplir les champs manquants");
                }
            }
            catch (WrongInformationException ex)
            {
                lblBadData.Text = ex.Message;
                lblBadData.Visible = true;
                txtPassword.Text = "";
            }
            catch (Exception ex)
            {
                lblBadData.Text = ex.Message;
                lblBadData.Visible = true;
                txtPassword.Text = "";
            }
        }
        /// <summary>
        /// This methods display the letters 1 second before they become "*"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TxtPassword_TextChanged(object sender, EventArgs e)
        {
            tmrPassword.Enabled = true;
            tmrPassword.Stop();
            tmrPassword.Start();
            TextBox textBox = sender as TextBox;
            password.Change(textBox);
        }

        private void TmrPassword_Tick(object sender, EventArgs e)
        {
            password.Tick(txtPassword);
            tmrPassword.Enabled = false;
        }

        private void imgUser_Click(object sender, EventArgs e)
        {
            FrmLoginTechnician frmAdminUser = new FrmLoginTechnician();
            frmAdminUser.Show();
            Visible = false;
        }
    }
}