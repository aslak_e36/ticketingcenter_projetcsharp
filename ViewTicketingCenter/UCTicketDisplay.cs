﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ViewTicketingCenter
{
    public partial class UCTicketDisplay : UserControl
    {
        public UCTicketDisplay(int id, string title, string category, string description, string priority, string dateStart, string dateEnd, string status, string owner, string technician)
        {
            InitializeComponent();
            lblTitleTicket.Text = title;
            lblCategoryTicket.Text = category;
            lblDescriptionTicket.Text = description;
            lblPriorityTicket.Text = priority;
            lblDateStartTicket.Text = dateStart;
            lblDateEndTicket.Text = dateEnd;
            lblStatusTicket.Text = status;
            lblUserTicket.Text = owner;
            lblTechnicianTicket.Text = technician;
        }
        public UCTicketDisplay()
        {
            InitializeComponent();
        }

        private void UCTicketDisplay_Load(object sender, EventArgs e)
        {

        }
    }
}
