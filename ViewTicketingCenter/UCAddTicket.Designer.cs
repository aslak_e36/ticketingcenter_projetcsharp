﻿namespace ViewTicketingCenter
{
    partial class UCAddTicket
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCategory = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.lblTextAddTicket = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.lblType = new System.Windows.Forms.Label();
            this.lblPriority = new System.Windows.Forms.Label();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.cmdCreateTicket = new System.Windows.Forms.Button();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.cmbType = new System.Windows.Forms.ComboBox();
            this.cmbCategory = new System.Windows.Forms.ComboBox();
            this.cmbPriority = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblCategory
            // 
            this.lblCategory.AutoSize = true;
            this.lblCategory.BackColor = System.Drawing.Color.Transparent;
            this.lblCategory.Font = new System.Drawing.Font("Berlin Sans FB", 10.5F);
            this.lblCategory.ForeColor = System.Drawing.Color.Salmon;
            this.lblCategory.Location = new System.Drawing.Point(352, 39);
            this.lblCategory.Name = "lblCategory";
            this.lblCategory.Size = new System.Drawing.Size(69, 16);
            this.lblCategory.TabIndex = 1;
            this.lblCategory.Text = "Categorie ";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.BackColor = System.Drawing.Color.Transparent;
            this.lblTitle.Font = new System.Drawing.Font("Berlin Sans FB", 10.5F);
            this.lblTitle.ForeColor = System.Drawing.Color.Salmon;
            this.lblTitle.Location = new System.Drawing.Point(48, 39);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(34, 16);
            this.lblTitle.TabIndex = 2;
            this.lblTitle.Text = "Titre";
            // 
            // lblTextAddTicket
            // 
            this.lblTextAddTicket.BackColor = System.Drawing.Color.Transparent;
            this.lblTextAddTicket.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.lblTextAddTicket.ForeColor = System.Drawing.Color.Salmon;
            this.lblTextAddTicket.Location = new System.Drawing.Point(0, 0);
            this.lblTextAddTicket.Name = "lblTextAddTicket";
            this.lblTextAddTicket.Size = new System.Drawing.Size(656, 23);
            this.lblTextAddTicket.TabIndex = 3;
            this.lblTextAddTicket.Text = "Veuillez remplir les informations nécessaires pour la création de votre ticket. M" +
    "erci.";
            this.lblTextAddTicket.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.BackColor = System.Drawing.Color.Transparent;
            this.lblDescription.Font = new System.Drawing.Font("Berlin Sans FB", 10.5F);
            this.lblDescription.ForeColor = System.Drawing.Color.Salmon;
            this.lblDescription.Location = new System.Drawing.Point(49, 161);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(71, 16);
            this.lblDescription.TabIndex = 4;
            this.lblDescription.Text = "Description";
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.BackColor = System.Drawing.Color.Transparent;
            this.lblType.Font = new System.Drawing.Font("Berlin Sans FB", 10.5F);
            this.lblType.ForeColor = System.Drawing.Color.Salmon;
            this.lblType.Location = new System.Drawing.Point(353, 98);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(37, 16);
            this.lblType.TabIndex = 5;
            this.lblType.Text = "Type";
            // 
            // lblPriority
            // 
            this.lblPriority.AutoSize = true;
            this.lblPriority.BackColor = System.Drawing.Color.Transparent;
            this.lblPriority.Font = new System.Drawing.Font("Berlin Sans FB", 10.5F);
            this.lblPriority.ForeColor = System.Drawing.Color.Salmon;
            this.lblPriority.Location = new System.Drawing.Point(48, 98);
            this.lblPriority.Name = "lblPriority";
            this.lblPriority.Size = new System.Drawing.Size(50, 16);
            this.lblPriority.TabIndex = 6;
            this.lblPriority.Text = "Priorité";
            // 
            // txtTitle
            // 
            this.txtTitle.Location = new System.Drawing.Point(51, 63);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(239, 23);
            this.txtTitle.TabIndex = 12;
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(53, 184);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(543, 100);
            this.txtDescription.TabIndex = 16;
            // 
            // cmdCreateTicket
            // 
            this.cmdCreateTicket.BackColor = System.Drawing.Color.White;
            this.cmdCreateTicket.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.cmdCreateTicket.Dock = System.Windows.Forms.DockStyle.Left;
            this.cmdCreateTicket.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCreateTicket.Location = new System.Drawing.Point(0, 0);
            this.cmdCreateTicket.Name = "cmdCreateTicket";
            this.cmdCreateTicket.Size = new System.Drawing.Size(300, 44);
            this.cmdCreateTicket.TabIndex = 17;
            this.cmdCreateTicket.Text = "Créer ticket";
            this.cmdCreateTicket.UseVisualStyleBackColor = false;
            this.cmdCreateTicket.Click += new System.EventHandler(this.cmdCreateTicket_Click);
            // 
            // cmdCancel
            // 
            this.cmdCancel.BackColor = System.Drawing.Color.Salmon;
            this.cmdCancel.Dock = System.Windows.Forms.DockStyle.Right;
            this.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCancel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cmdCancel.Location = new System.Drawing.Point(320, 0);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(306, 44);
            this.cmdCancel.TabIndex = 18;
            this.cmdCancel.Text = "Annuler";
            this.cmdCancel.UseVisualStyleBackColor = false;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // cmbType
            // 
            this.cmbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbType.FormattingEnabled = true;
            this.cmbType.Items.AddRange(new object[] {
            "Audio-visuel",
            "Autres",
            "Gestion des comptes et messagries utilisateurs",
            "Logiciel",
            "Serveur",
            "Matériel",
            "Réseaux",
            "Restauration-Sauvegarde",
            "Site Internet",
            "SIte Intranet"});
            this.cmbType.Location = new System.Drawing.Point(355, 63);
            this.cmbType.Name = "cmbType";
            this.cmbType.Size = new System.Drawing.Size(240, 24);
            this.cmbType.TabIndex = 13;
            // 
            // cmbCategory
            // 
            this.cmbCategory.DisplayMember = "(none)";
            this.cmbCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCategory.FormattingEnabled = true;
            this.cmbCategory.Items.AddRange(new object[] {
            "Trés Basse",
            "Basse",
            "Moyenne",
            "Haute",
            "Trés Haute"});
            this.cmbCategory.Location = new System.Drawing.Point(51, 121);
            this.cmbCategory.Name = "cmbCategory";
            this.cmbCategory.Size = new System.Drawing.Size(242, 24);
            this.cmbCategory.TabIndex = 14;
            // 
            // cmbPriority
            // 
            this.cmbPriority.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPriority.FormattingEnabled = true;
            this.cmbPriority.Items.AddRange(new object[] {
            "Incident",
            "Demande"});
            this.cmbPriority.Location = new System.Drawing.Point(356, 121);
            this.cmbPriority.Name = "cmbPriority";
            this.cmbPriority.Size = new System.Drawing.Size(242, 24);
            this.cmbPriority.TabIndex = 15;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cmdCreateTicket);
            this.panel1.Controls.Add(this.cmdCancel);
            this.panel1.Location = new System.Drawing.Point(15, 313);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(626, 44);
            this.panel1.TabIndex = 19;
            // 
            // UCAddTicket
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.cmbPriority);
            this.Controls.Add(this.cmbCategory);
            this.Controls.Add(this.cmbType);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.txtTitle);
            this.Controls.Add(this.lblPriority);
            this.Controls.Add(this.lblType);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.lblTextAddTicket);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.lblCategory);
            this.Font = new System.Drawing.Font("Berlin Sans FB", 10.5F);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximumSize = new System.Drawing.Size(656, 364);
            this.MinimumSize = new System.Drawing.Size(656, 364);
            this.Name = "UCAddTicket";
            this.Size = new System.Drawing.Size(656, 364);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblCategory;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblTextAddTicket;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.Label lblPriority;
        private System.Windows.Forms.TextBox txtTitle;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Button cmdCreateTicket;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.ComboBox cmbType;
        private System.Windows.Forms.ComboBox cmbCategory;
        private System.Windows.Forms.ComboBox cmbPriority;
        private System.Windows.Forms.Panel panel1;
    }
}
