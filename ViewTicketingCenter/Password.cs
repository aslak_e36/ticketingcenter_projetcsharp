﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ViewTicketingCenter;

namespace ViewTicketingCenter
{
    /// <summary>
    /// class that manages the behavior of the the password checkboxes
    /// </summary>
    public class Password
    {
        /// <summary>
        /// stores the password writen on the textbox
        /// </summary>
        private string currentPassword = "";
        public string CurrentPassword
        {
            get
            {
                return currentPassword;
            }
        }
        /// <summary>
        /// This fonction must be called when a textbox is changed
        /// Fonction that adds wildcard to all but the last letter, it also manages adding or removing a letter
        /// Bug: you cant remove multiple characters at once by selecting more then one character and then removing it
        /// </summary>
        /// <param name="sender">textbox that we want to change</param>
        public void Change(TextBox sender)
        {

            if (sender.Text != null)
            {
                string inputString = sender.Text;
                // Adds the new letter to the global variable currentPassword and then masks the original text box used
                if (inputString.Length > currentPassword.Length)
                {
                    var newChar = inputString.Substring(inputString.Length - 1);
                    currentPassword += newChar;
                    var newBoxText = "";
                    for (int i = 0; i < currentPassword.Length - 1; i++)
                    {
                        newBoxText += "*";
                    }
                    newBoxText += newChar;
                    sender.Text = newBoxText;
                    sender.SelectionStart = sender.Text.Count();
                }
                else if ((inputString.Length < currentPassword.Length) && (inputString.Length > 0)) // Removes characters from the currentPassword field til it matches the length of the textbox field and then exposes a single character at the end
                {
                    currentPassword = currentPassword.Remove(inputString.Length, currentPassword.Length - inputString.Length);
                    var newBoxText = "";
                    for (int i = 0; i < currentPassword.Length; i++)
                    {
                        newBoxText += "*";
                    }
                    sender.Text = newBoxText;
                    sender.SelectionStart = sender.Text.Count();
                }
            }
        }
        /// <summary>
        /// This fonction must be called on the timer after changing a character
        /// Fonction that turns all the textbox's text to wildcards
        /// </summary>
        /// <param name="sender">textbox that we want to change</param>
        public void Tick(TextBox sender)
        {
            string text = "";
            for (int i = 0; i < sender.Text.Count(); i++)
            {
                text = text + "*";
            }
            sender.Text = text;
            sender.SelectionStart = sender.Text.Count();
        }
    }
}
