﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ModelTicketingCenter;

namespace ViewTicketingCenter
{
    public partial class FrmLoginTechnician : Form
    {
        private Technician technician = new Technician();
        private Password password = new Password();
        private JsonReader json;
        public FrmLoginTechnician()
        {
            InitializeComponent();
        }
        /// <summary>
        /// This method will display the Login page for normal users
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdUserLogin_Click(object sender, EventArgs e)
        {
            FrmLogin frmLogin = new FrmLogin();
            frmLogin.Show();
            Visible = false;
        }
        /// <summary>
        /// This method will close the application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        /// <summary>
        /// This method will minimize the application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        /// <summary>
        /// This method will check for the login
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdLogin_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtEmail.Text != "" && txtPassword.Text != "")
                {
                    if (Login.LoginTechnician(txtEmail.Text, password.CurrentPassword, this.technician))
                    {
                        if (this.technician.Type == 0)
                        {
                            FrmHomeAdmin frmHomeAdmin = new FrmHomeAdmin(this.technician);
                            frmHomeAdmin.Show();
                            Visible = false;
                            json.Write(lblEmail.Text);
                        }
                        else
                        {
                            FrmHomeTechnician frmHomeTechnician = new FrmHomeTechnician(this.technician);
                            frmHomeTechnician.Show();
                            Visible = false;
                            json.Write(lblEmail.Text);
                        }
                    }
                }
                else
                {
                    throw new Exception("Veuillez remplir les champs manquants");
                }
            }
            catch (WrongInformationException ex)
            {
                lblBadData.Text = ex.Message;
                lblBadData.Visible = true;
                txtPassword.Text = "";
            }
            catch (Exception ex)
            {
                lblBadData.Text = ex.Message;
                lblBadData.Visible = true;
                txtPassword.Text = "";
            }
        }
    }
}
