﻿namespace ViewTicketingCenter
{
    partial class FrmRegister
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRegister));
            this.btnClose = new System.Windows.Forms.Button();
            this.btnMinimize = new System.Windows.Forms.Button();
            this.imgLogo = new System.Windows.Forms.PictureBox();
            this.lblInscription = new System.Windows.Forms.Label();
            this.lblLogin = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.lblPassword = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.txtPasswordVerification = new System.Windows.Forms.TextBox();
            this.lblPasswordVerification = new System.Windows.Forms.Label();
            this.cmdRegister = new System.Windows.Forms.Button();
            this.lblBadData = new System.Windows.Forms.Label();
            this.tmrPassword = new System.Windows.Forms.Timer(this.components);
            this.tmrPasswordVerification = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.imgLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.Red;
            this.btnClose.Location = new System.Drawing.Point(592, 1);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(52, 39);
            this.btnClose.TabIndex = 9;
            this.btnClose.Text = "x";
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // btnMinimize
            // 
            this.btnMinimize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.btnMinimize.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMinimize.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinimize.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMinimize.ForeColor = System.Drawing.Color.White;
            this.btnMinimize.Location = new System.Drawing.Point(541, 1);
            this.btnMinimize.Name = "btnMinimize";
            this.btnMinimize.Size = new System.Drawing.Size(52, 39);
            this.btnMinimize.TabIndex = 10;
            this.btnMinimize.Text = "-";
            this.btnMinimize.UseVisualStyleBackColor = false;
            this.btnMinimize.Click += new System.EventHandler(this.BtnMinimize_Click);
            // 
            // imgLogo
            // 
            this.imgLogo.Image = ((System.Drawing.Image)(resources.GetObject("imgLogo.Image")));
            this.imgLogo.Location = new System.Drawing.Point(86, 42);
            this.imgLogo.Name = "imgLogo";
            this.imgLogo.Size = new System.Drawing.Size(463, 184);
            this.imgLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgLogo.TabIndex = 11;
            this.imgLogo.TabStop = false;
            // 
            // lblInscription
            // 
            this.lblInscription.AutoSize = true;
            this.lblInscription.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInscription.ForeColor = System.Drawing.Color.Salmon;
            this.lblInscription.Location = new System.Drawing.Point(257, 157);
            this.lblInscription.Name = "lblInscription";
            this.lblInscription.Size = new System.Drawing.Size(120, 23);
            this.lblInscription.TabIndex = 12;
            this.lblInscription.Text = "INSCRIPTION";
            // 
            // lblLogin
            // 
            this.lblLogin.AutoSize = true;
            this.lblLogin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblLogin.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLogin.ForeColor = System.Drawing.Color.Salmon;
            this.lblLogin.Location = new System.Drawing.Point(122, 440);
            this.lblLogin.Name = "lblLogin";
            this.lblLogin.Size = new System.Drawing.Size(387, 23);
            this.lblLogin.TabIndex = 18;
            this.lblLogin.Text = "Vous avez déjà un compte ? Connectez-vous !";
            this.lblLogin.Click += new System.EventHandler(this.LblLogin_Click);
            this.lblLogin.MouseEnter += new System.EventHandler(this.LblLogin_MouseEnter);
            this.lblLogin.MouseLeave += new System.EventHandler(this.LblLogin_MouseLeave);
            // 
            // txtPassword
            // 
            this.txtPassword.BackColor = System.Drawing.Color.Salmon;
            this.txtPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPassword.Location = new System.Drawing.Point(333, 302);
            this.txtPassword.Margin = new System.Windows.Forms.Padding(2, 10, 2, 10);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(216, 24);
            this.txtPassword.TabIndex = 16;
            this.txtPassword.TextChanged += new System.EventHandler(this.TxtPassword_TextChanged);
            // 
            // txtEmail
            // 
            this.txtEmail.BackColor = System.Drawing.Color.Salmon;
            this.txtEmail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail.Location = new System.Drawing.Point(333, 260);
            this.txtEmail.Margin = new System.Windows.Forms.Padding(2, 10, 2, 10);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(216, 24);
            this.txtEmail.TabIndex = 15;
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPassword.ForeColor = System.Drawing.Color.Salmon;
            this.lblPassword.Location = new System.Drawing.Point(82, 301);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(117, 23);
            this.lblPassword.TabIndex = 14;
            this.lblPassword.Text = "Mot de passe";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.ForeColor = System.Drawing.Color.Salmon;
            this.lblEmail.Location = new System.Drawing.Point(82, 259);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(56, 23);
            this.lblEmail.TabIndex = 13;
            this.lblEmail.Text = "Email";
            // 
            // txtPasswordVerification
            // 
            this.txtPasswordVerification.BackColor = System.Drawing.Color.Salmon;
            this.txtPasswordVerification.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPasswordVerification.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPasswordVerification.Location = new System.Drawing.Point(333, 344);
            this.txtPasswordVerification.Margin = new System.Windows.Forms.Padding(2, 10, 2, 10);
            this.txtPasswordVerification.Name = "txtPasswordVerification";
            this.txtPasswordVerification.Size = new System.Drawing.Size(216, 24);
            this.txtPasswordVerification.TabIndex = 20;
            this.txtPasswordVerification.TextChanged += new System.EventHandler(this.TxtPasswordVerification_TextChanged);
            // 
            // lblPasswordVerification
            // 
            this.lblPasswordVerification.AutoSize = true;
            this.lblPasswordVerification.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPasswordVerification.ForeColor = System.Drawing.Color.Salmon;
            this.lblPasswordVerification.Location = new System.Drawing.Point(82, 344);
            this.lblPasswordVerification.Name = "lblPasswordVerification";
            this.lblPasswordVerification.Size = new System.Drawing.Size(229, 23);
            this.lblPasswordVerification.TabIndex = 19;
            this.lblPasswordVerification.Text = "Mot de passe (Vérification)";
            // 
            // cmdRegister
            // 
            this.cmdRegister.BackColor = System.Drawing.Color.Salmon;
            this.cmdRegister.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdRegister.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.cmdRegister.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRegister.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdRegister.Location = new System.Drawing.Point(84, 394);
            this.cmdRegister.Name = "cmdRegister";
            this.cmdRegister.Size = new System.Drawing.Size(463, 43);
            this.cmdRegister.TabIndex = 21;
            this.cmdRegister.Text = "Valider mon inscription";
            this.cmdRegister.UseVisualStyleBackColor = false;
            this.cmdRegister.Click += new System.EventHandler(this.CmdRegister_Click);
            // 
            // lblBadData
            // 
            this.lblBadData.AutoSize = true;
            this.lblBadData.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.lblBadData.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBadData.ForeColor = System.Drawing.Color.OrangeRed;
            this.lblBadData.Location = new System.Drawing.Point(12, 212);
            this.lblBadData.MaximumSize = new System.Drawing.Size(620, 1000);
            this.lblBadData.MinimumSize = new System.Drawing.Size(620, 40);
            this.lblBadData.Name = "lblBadData";
            this.lblBadData.Size = new System.Drawing.Size(620, 40);
            this.lblBadData.TabIndex = 22;
            this.lblBadData.Text = "Les informations saisies sont incorrectes";
            this.lblBadData.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblBadData.Visible = false;
            // 
            // tmrPassword
            // 
            this.tmrPassword.Interval = 1000;
            this.tmrPassword.Tick += new System.EventHandler(this.TmrPassword_Tick);
            // 
            // tmrPasswordVerification
            // 
            this.tmrPasswordVerification.Interval = 1000;
            this.tmrPasswordVerification.Tick += new System.EventHandler(this.TmrPasswordVerification_Tick);
            // 
            // FrmRegister
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.ClientSize = new System.Drawing.Size(645, 498);
            this.Controls.Add(this.lblBadData);
            this.Controls.Add(this.cmdRegister);
            this.Controls.Add(this.txtPasswordVerification);
            this.Controls.Add(this.lblPasswordVerification);
            this.Controls.Add(this.lblLogin);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.lblPassword);
            this.Controls.Add(this.lblEmail);
            this.Controls.Add(this.lblInscription);
            this.Controls.Add(this.imgLogo);
            this.Controls.Add(this.btnMinimize);
            this.Controls.Add(this.btnClose);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmRegister";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmInscription";
            ((System.ComponentModel.ISupportInitialize)(this.imgLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnMinimize;
        private System.Windows.Forms.PictureBox imgLogo;
        private System.Windows.Forms.Label lblInscription;
        private System.Windows.Forms.Label lblLogin;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.TextBox txtPasswordVerification;
        private System.Windows.Forms.Label lblPasswordVerification;
        private System.Windows.Forms.Button cmdRegister;
        private System.Windows.Forms.Label lblBadData;
        private System.Windows.Forms.Timer tmrPassword;
        private System.Windows.Forms.Timer tmrPasswordVerification;
    }
}