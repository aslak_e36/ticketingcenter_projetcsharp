﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ViewTicketingCenter
{
    public partial class UCTickets : UserControl
    {
        /// <summary>
        /// Constructor for UCTickets
        /// </summary>
        public UCTickets()
        {
        }
        /// <summary>
        /// Constructor for UCTickets
        /// </summary>
        /// <param name="id"></param>
        /// <param name="title"></param>
        /// <param name="category"></param>
        /// <param name="description"></param>
        /// <param name="priority"></param>
        /// <param name="dateStart"></param>
        /// <param name="dateEnd"></param>
        /// <param name="status"></param>
        /// <param name="owner"></param>
        /// <param name="technician"></param>
        public UCTickets(int id, string title, string category, string description, string priority, string dateStart, string dateEnd, string status, string owner, string technician)
        {
            InitializeComponent();
            lblTitleTicket.Text = title;
            lblCategoryTicket.Text = category;
            lblDescriptionTicket.Text = description;
            lblPriorityTicket.Text = priority;
            lblDateStartTicket.Text = dateStart;
            lblDateEndTicket.Text = dateEnd;
            lblStatusTicket.Text = status;
            lblUserTicket.Text = owner;
            lblTechnicianTicket.Text = technician;
        }
    }
}