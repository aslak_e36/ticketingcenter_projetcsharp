﻿namespace ViewTicketingCenter
{
    partial class UCTicketDisplay
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTitle = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.lblPriority = new System.Windows.Forms.Label();
            this.lblDateStart = new System.Windows.Forms.Label();
            this.lblDateEnd = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblUser = new System.Windows.Forms.Label();
            this.lblTechnician = new System.Windows.Forms.Label();
            this.lblTitleTicket = new System.Windows.Forms.Label();
            this.lblUserTicket = new System.Windows.Forms.Label();
            this.lblPriorityTicket = new System.Windows.Forms.Label();
            this.lblTechnicianTicket = new System.Windows.Forms.Label();
            this.lblDateStartTicket = new System.Windows.Forms.Label();
            this.lblDateEndTicket = new System.Windows.Forms.Label();
            this.lblStatusTicket = new System.Windows.Forms.Label();
            this.lblDescriptionTicket = new System.Windows.Forms.Label();
            this.lblCategorie = new System.Windows.Forms.Label();
            this.lblCategoryTicket = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Berlin Sans FB", 10.25F);
            this.lblTitle.Location = new System.Drawing.Point(16, 14);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(34, 16);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "Titre";
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Font = new System.Drawing.Font("Berlin Sans FB", 10.25F);
            this.lblDescription.Location = new System.Drawing.Point(16, 66);
            this.lblDescription.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(71, 16);
            this.lblDescription.TabIndex = 1;
            this.lblDescription.Text = "Description";
            // 
            // lblPriority
            // 
            this.lblPriority.AutoSize = true;
            this.lblPriority.Font = new System.Drawing.Font("Berlin Sans FB", 10.25F);
            this.lblPriority.Location = new System.Drawing.Point(248, 14);
            this.lblPriority.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPriority.Name = "lblPriority";
            this.lblPriority.Size = new System.Drawing.Size(50, 16);
            this.lblPriority.TabIndex = 2;
            this.lblPriority.Text = "Priorité";
            // 
            // lblDateStart
            // 
            this.lblDateStart.AutoSize = true;
            this.lblDateStart.Font = new System.Drawing.Font("Berlin Sans FB", 10.25F);
            this.lblDateStart.Location = new System.Drawing.Point(428, 14);
            this.lblDateStart.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDateStart.Name = "lblDateStart";
            this.lblDateStart.Size = new System.Drawing.Size(108, 16);
            this.lblDateStart.TabIndex = 3;
            this.lblDateStart.Text = "Date d\'ouverture";
            // 
            // lblDateEnd
            // 
            this.lblDateEnd.AutoSize = true;
            this.lblDateEnd.Font = new System.Drawing.Font("Berlin Sans FB", 10.25F);
            this.lblDateEnd.Location = new System.Drawing.Point(428, 41);
            this.lblDateEnd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDateEnd.Name = "lblDateEnd";
            this.lblDateEnd.Size = new System.Drawing.Size(74, 16);
            this.lblDateEnd.TabIndex = 4;
            this.lblDateEnd.Text = "Date de fin";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("Berlin Sans FB", 10.25F);
            this.lblStatus.Location = new System.Drawing.Point(662, 14);
            this.lblStatus.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(44, 16);
            this.lblStatus.TabIndex = 5;
            this.lblStatus.Text = "Status";
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = true;
            this.lblUser.Font = new System.Drawing.Font("Berlin Sans FB", 10.25F);
            this.lblUser.Location = new System.Drawing.Point(16, 41);
            this.lblUser.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(64, 16);
            this.lblUser.TabIndex = 6;
            this.lblUser.Text = "Utilisateur";
            // 
            // lblTechnician
            // 
            this.lblTechnician.AutoSize = true;
            this.lblTechnician.Font = new System.Drawing.Font("Berlin Sans FB", 10.25F);
            this.lblTechnician.Location = new System.Drawing.Point(248, 41);
            this.lblTechnician.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTechnician.Name = "lblTechnician";
            this.lblTechnician.Size = new System.Drawing.Size(66, 16);
            this.lblTechnician.TabIndex = 7;
            this.lblTechnician.Text = "Technicien";
            // 
            // lblTitleTicket
            // 
            this.lblTitleTicket.AutoSize = true;
            this.lblTitleTicket.Font = new System.Drawing.Font("Berlin Sans FB", 10.25F);
            this.lblTitleTicket.Location = new System.Drawing.Point(58, 14);
            this.lblTitleTicket.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTitleTicket.Name = "lblTitleTicket";
            this.lblTitleTicket.Size = new System.Drawing.Size(17, 16);
            this.lblTitleTicket.TabIndex = 8;
            this.lblTitleTicket.Text = "...";
            // 
            // lblUserTicket
            // 
            this.lblUserTicket.AutoSize = true;
            this.lblUserTicket.Font = new System.Drawing.Font("Berlin Sans FB", 10.25F);
            this.lblUserTicket.Location = new System.Drawing.Point(88, 41);
            this.lblUserTicket.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUserTicket.Name = "lblUserTicket";
            this.lblUserTicket.Size = new System.Drawing.Size(17, 16);
            this.lblUserTicket.TabIndex = 9;
            this.lblUserTicket.Text = "...";
            // 
            // lblPriorityTicket
            // 
            this.lblPriorityTicket.AutoSize = true;
            this.lblPriorityTicket.Font = new System.Drawing.Font("Berlin Sans FB", 10.25F);
            this.lblPriorityTicket.Location = new System.Drawing.Point(306, 14);
            this.lblPriorityTicket.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPriorityTicket.Name = "lblPriorityTicket";
            this.lblPriorityTicket.Size = new System.Drawing.Size(17, 16);
            this.lblPriorityTicket.TabIndex = 10;
            this.lblPriorityTicket.Text = "...";
            // 
            // lblTechnicianTicket
            // 
            this.lblTechnicianTicket.AutoSize = true;
            this.lblTechnicianTicket.Font = new System.Drawing.Font("Berlin Sans FB", 10.25F);
            this.lblTechnicianTicket.Location = new System.Drawing.Point(322, 41);
            this.lblTechnicianTicket.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTechnicianTicket.Name = "lblTechnicianTicket";
            this.lblTechnicianTicket.Size = new System.Drawing.Size(17, 16);
            this.lblTechnicianTicket.TabIndex = 11;
            this.lblTechnicianTicket.Text = "...";
            // 
            // lblDateStartTicket
            // 
            this.lblDateStartTicket.AutoSize = true;
            this.lblDateStartTicket.Font = new System.Drawing.Font("Berlin Sans FB", 10.25F);
            this.lblDateStartTicket.Location = new System.Drawing.Point(544, 14);
            this.lblDateStartTicket.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDateStartTicket.Name = "lblDateStartTicket";
            this.lblDateStartTicket.Size = new System.Drawing.Size(17, 16);
            this.lblDateStartTicket.TabIndex = 12;
            this.lblDateStartTicket.Text = "...";
            // 
            // lblDateEndTicket
            // 
            this.lblDateEndTicket.AutoSize = true;
            this.lblDateEndTicket.Font = new System.Drawing.Font("Berlin Sans FB", 10.25F);
            this.lblDateEndTicket.Location = new System.Drawing.Point(510, 41);
            this.lblDateEndTicket.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDateEndTicket.Name = "lblDateEndTicket";
            this.lblDateEndTicket.Size = new System.Drawing.Size(17, 16);
            this.lblDateEndTicket.TabIndex = 13;
            this.lblDateEndTicket.Text = "...";
            // 
            // lblStatusTicket
            // 
            this.lblStatusTicket.AutoSize = true;
            this.lblStatusTicket.Font = new System.Drawing.Font("Berlin Sans FB", 10.25F);
            this.lblStatusTicket.Location = new System.Drawing.Point(714, 14);
            this.lblStatusTicket.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStatusTicket.Name = "lblStatusTicket";
            this.lblStatusTicket.Size = new System.Drawing.Size(17, 16);
            this.lblStatusTicket.TabIndex = 14;
            this.lblStatusTicket.Text = "...";
            // 
            // lblDescriptionTicket
            // 
            this.lblDescriptionTicket.AutoSize = true;
            this.lblDescriptionTicket.Font = new System.Drawing.Font("Berlin Sans FB", 10.25F);
            this.lblDescriptionTicket.Location = new System.Drawing.Point(16, 86);
            this.lblDescriptionTicket.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDescriptionTicket.Name = "lblDescriptionTicket";
            this.lblDescriptionTicket.Size = new System.Drawing.Size(17, 16);
            this.lblDescriptionTicket.TabIndex = 15;
            this.lblDescriptionTicket.Text = "...";
            // 
            // lblCategorie
            // 
            this.lblCategorie.AutoSize = true;
            this.lblCategorie.Font = new System.Drawing.Font("Berlin Sans FB", 10.25F);
            this.lblCategorie.Location = new System.Drawing.Point(662, 41);
            this.lblCategorie.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCategorie.Name = "lblCategorie";
            this.lblCategorie.Size = new System.Drawing.Size(65, 16);
            this.lblCategorie.TabIndex = 16;
            this.lblCategorie.Text = "Categorie";
            // 
            // lblCategoryTicket
            // 
            this.lblCategoryTicket.AutoSize = true;
            this.lblCategoryTicket.Font = new System.Drawing.Font("Berlin Sans FB", 10.25F);
            this.lblCategoryTicket.Location = new System.Drawing.Point(735, 41);
            this.lblCategoryTicket.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCategoryTicket.Name = "lblCategoryTicket";
            this.lblCategoryTicket.Size = new System.Drawing.Size(17, 16);
            this.lblCategoryTicket.TabIndex = 17;
            this.lblCategoryTicket.Text = "...";
            // 
            // UCTicketDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Salmon;
            this.Controls.Add(this.lblCategoryTicket);
            this.Controls.Add(this.lblCategorie);
            this.Controls.Add(this.lblDescriptionTicket);
            this.Controls.Add(this.lblStatusTicket);
            this.Controls.Add(this.lblDateEndTicket);
            this.Controls.Add(this.lblDateStartTicket);
            this.Controls.Add(this.lblTechnicianTicket);
            this.Controls.Add(this.lblPriorityTicket);
            this.Controls.Add(this.lblUserTicket);
            this.Controls.Add(this.lblTitleTicket);
            this.Controls.Add(this.lblTechnician);
            this.Controls.Add(this.lblUser);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.lblDateEnd);
            this.Controls.Add(this.lblDateStart);
            this.Controls.Add(this.lblPriority);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.lblTitle);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "UCTicketDisplay";
            this.Size = new System.Drawing.Size(857, 156);
            this.Load += new System.EventHandler(this.UCTicketDisplay_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Label lblPriority;
        private System.Windows.Forms.Label lblDateStart;
        private System.Windows.Forms.Label lblDateEnd;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label lblUser;
        private System.Windows.Forms.Label lblTechnician;
        private System.Windows.Forms.Label lblTitleTicket;
        private System.Windows.Forms.Label lblUserTicket;
        private System.Windows.Forms.Label lblPriorityTicket;
        private System.Windows.Forms.Label lblTechnicianTicket;
        private System.Windows.Forms.Label lblDateStartTicket;
        private System.Windows.Forms.Label lblDateEndTicket;
        private System.Windows.Forms.Label lblStatusTicket;
        private System.Windows.Forms.Label lblDescriptionTicket;
        private System.Windows.Forms.Label lblCategorie;
        private System.Windows.Forms.Label lblCategoryTicket;
    }
}
