﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ModelTicketingCenter;

namespace ViewTicketingCenter
{
    public partial class FrmHomeTechnician : Form
    {
        private List<Ticket> tickets = new List<Ticket>();
        private Technician technician;
        
        /// <summary>
        /// Constructor for FrmHomeTechnician 
        /// </summary>
        /// <param name="technician"></param>
        public FrmHomeTechnician(Technician technician)
        {
            this.technician = new Technician();
            this.technician = technician;
            InitializeComponent();
        }
        /// <summary>
        /// This method will close the application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        /// <summary>
        /// This methods will minimize the application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        /// <summary>
        /// This method will restart the application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdDisconnect_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }
        /// <summary>
        /// This methhods will load all tickets in database and display them
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmHomeTechnician_Load(object sender, EventArgs e)
        {
            lblEmail.Text = technician.Email;
            UCTechnicianTickets oldTicket = new UCTechnicianTickets();
            oldTicket.InitTech(this.technician);
            this.tickets = TicketTable.GetListOfTickets();

            lblEmail.Text = technician.Email;
            pnlTicketDisplay.Controls.Add(oldTicket);

            this.Dock = DockStyle.Fill;

            foreach (var ticket in this.tickets)
            {
                UCTechnicianTickets ucTicket = new UCTechnicianTickets(ticket.Id, ticket.Title, ticket.Category, ticket.Description, ticket.Priority, ticket.DateStart, ticket.DateEnd, ticket.Status, ticket.Owner, ticket.Technician);
                ucTicket.InitTech(this.technician);
                ucTicket.Top = oldTicket.Bottom + 3;
                pnlTicketDisplay.Controls.Add(ucTicket);
                pnlTicketDisplay.VerticalScroll.Value = pnlTicketDisplay.VerticalScroll.Maximum;
                oldTicket = ucTicket;
            }
        }
        /// <summary>
        /// This methods shows the tickets when you click on the "Tickets" button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdTickets_Click(object sender, EventArgs e)
        {
            UCTechnicianTickets oldTicket = new UCTechnicianTickets();
            this.tickets = TicketTable.GetListOfTickets();

            lblEmail.Text = technician.Email;
            pnlTicketDisplay.Controls.Add(oldTicket);

            this.Dock = DockStyle.Fill;

            foreach (var ticket in this.tickets)
            {
                UCTechnicianTickets ucTicket = new UCTechnicianTickets(ticket.Id, ticket.Title, ticket.Category, ticket.Description, ticket.Priority, ticket.DateStart, ticket.DateEnd, ticket.Status, ticket.Owner, ticket.Technician);
                ucTicket.Top = oldTicket.Bottom + 3;
                pnlTicketDisplay.Controls.Add(ucTicket);
                pnlTicketDisplay.VerticalScroll.Value = pnlTicketDisplay.VerticalScroll.Maximum;
                oldTicket = ucTicket;
            }
        }
        /// <summary>
        /// This methods shows the tickets when you click on the "Mes Tickets" button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdMyTickets_Click(object sender, EventArgs e)
        {
            UCTechnicianTickets oldTicket = new UCTechnicianTickets();
            this.tickets = TicketTable.GetTechTickets(this.technician.Id);

            pnlTicketDisplay.Controls.Clear();
            pnlTicketDisplay.Controls.Add(oldTicket);

            this.Dock = DockStyle.Fill;
            foreach (var ticket in this.tickets)
            {
                UCTechnicianTickets ucTicket = new UCTechnicianTickets(ticket.Id, ticket.Title, ticket.Category, ticket.Description, ticket.Priority, ticket.DateStart, ticket.DateEnd, ticket.Status, ticket.Owner, ticket.Technician);
                ucTicket.Top = oldTicket.Bottom + 3;
                pnlTicketDisplay.Controls.Add(ucTicket);
                pnlTicketDisplay.VerticalScroll.Value = pnlTicketDisplay.VerticalScroll.Maximum;
                oldTicket = ucTicket;
            }
        }
    }
}
