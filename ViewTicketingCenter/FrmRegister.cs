﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using ModelTicketingCenter;


namespace ViewTicketingCenter
{
    public partial class FrmRegister : Form
    {
        private User user = new User();
        Password password = new Password();
        Password passwordVerfication = new Password();
        const string ValidEmailAddressPattern = "^[a-z0-9][-a-z0-9._]+@([-a-z0-9]+.)+[a-z]{2,5}$";
        const string ValidPasswordPattern = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$";

        public FrmRegister()
        {
            InitializeComponent();
        }
        /// <summary>
        /// This function closes the application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnClose_Click(object sender, EventArgs e)
        {
            FrmLogin frmLogin = new FrmLogin();
            frmLogin.Show();
            Visible = false;
        }
        /// <summary>
        /// This function minimizes the application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        /// <summary>
        /// This function underlines the text in the lblLogin when you have your mouse on it
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LblLogin_MouseEnter(object sender, EventArgs e)
        {
            lblLogin.Font = new Font(lblLogin.Font.Name, lblLogin.Font.SizeInPoints, FontStyle.Underline);
        }
        /// <summary>
        /// This function remove underlined text in the lblLogin when you leave your mouse on it
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LblLogin_MouseLeave(object sender, EventArgs e)
        {
            lblLogin.Font = new Font(lblLogin.Font.Name, lblLogin.Font.SizeInPoints, FontStyle.Regular);
        }
        /// <summary>
        /// This function open login's window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LblLogin_Click(object sender, EventArgs e)
        {
            FrmLogin frmLogin = new FrmLogin();
            frmLogin.Show();
            Visible = false;
        }
        /// <summary>
        /// This function register the person if all parameters are OK to register
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdRegister_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtPassword.Text != txtPasswordVerification.Text)
                {
                    throw new Exception("Les mots de passe ne se correspondent pas entre eux!");
                }
                else if (IsEmailValid(txtEmail.Text) && txtEmail.Text != "" && isPasswordValid(password.CurrentPassword) && txtPassword.Text != "" && txtPasswordVerification.Text != "" && txtPassword.Text == txtPasswordVerification.Text)
                {
                    if (Register.RegisterUser(txtEmail.Text, password.CurrentPassword, this.user))
                    {
                        FrmHome frmHome = new FrmHome(this.user);
                        frmHome.Show();
                        Visible = false;
                    }
                }
                else
                {
                    throw new Exception("Les informations relevées ne sont pas conformes!");
                }
            }
            catch(ExistingEmailException ex)
            {
                lblBadData.Text = ex.Message;
                lblBadData.Visible = true;
                txtEmail.Text = "";
                txtPassword.Text = "";
                txtPasswordVerification.Text = "";
            }
            catch (Exception ex)
            {
                lblBadData.Text = ex.Message;
                lblBadData.Visible = true;
                txtPassword.Text = "";
                txtPasswordVerification.Text = "";
            }
        }
        /// <summary>
        /// This method watch if the regular expression for email is OK
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static bool IsEmailValid(string email)
        {
            var regex = new Regex(ValidEmailAddressPattern, RegexOptions.IgnoreCase);
            if (regex.IsMatch(email))
            {
                return true;
            }
            else
            {
                throw new Exception("Email pas valide ou manquant! Voici un exemple : votrenom@exemple.com");
            }
        }
        /// <summary>
        /// This method watch if the regular expression for password is OK
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        public static bool isPasswordValid(string password)
        {
            var regex = new Regex(ValidPasswordPattern, RegexOptions.IgnoreCase);
            if (regex.IsMatch(password))
            {
                return true;
            }
            else
            {
                throw new Exception("Password pas conforme ou manquant! Il faut au moins un chiffre, une masjucule, un caractère spéciale et aussi 8 caractères minimum!");
            }
        }

        private void TxtPassword_TextChanged(object sender, EventArgs e)
        {
            tmrPassword.Enabled = true;
            tmrPassword.Stop();
            tmrPassword.Start();
            TextBox textBox = sender as TextBox;
            password.Change(textBox);
        }

        private void TmrPassword_Tick(object sender, EventArgs e)
        {
            password.Tick(txtPassword);
            tmrPassword.Enabled = false;
        }

        private void TxtPasswordVerification_TextChanged(object sender, EventArgs e)
        {
            tmrPasswordVerification.Enabled = true;
            tmrPasswordVerification.Stop();
            tmrPasswordVerification.Start();
            TextBox textBox = sender as TextBox;
            passwordVerfication.Change(textBox);
        }

        private void TmrPasswordVerification_Tick(object sender, EventArgs e)
        {
            passwordVerfication.Tick(txtPasswordVerification);
            tmrPasswordVerification.Enabled = false;
        }
    }
}
