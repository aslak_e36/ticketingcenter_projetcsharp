﻿using System.Text;
using System.Text.RegularExpressions;

public enum PasswordScore
{
    Blank = 0,
    VeryWeak = 1,
    Weak = 2,
    Medium = 3,
    Strong = 4,
    VeryStrong = 5
}

public class PasswordAdvisor
{
    static int score = 0;
    public static PasswordScore CheckStrength(string password)
    {
        if (password.Length < 1)
        {
            return PasswordScore.Blank;
        }
        else if (password.Length < 3)
        {
            return PasswordScore.VeryWeak;
        }
        else if (password.Length < 5)
        {
            return PasswordScore.Weak;
        }
        else if (password.Length < 7)
        {
            return PasswordScore.Medium;
        }
        else if (password.Length < 9)
        {
            return PasswordScore.Strong;
        }
        else if (password.Length < 11)
        {
            return PasswordScore.VeryStrong;
        }
        else
        {
            return PasswordScore.VeryStrong;
        }

        return (PasswordScore)score;
    }
}