﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ModelTicketingCenter;

namespace ViewTicketingCenter
{
    public partial class FrmTechnician : Form
    {
        private Technician technician = new Technician();
        private Password password = new Password();
        private JsonReader json;
        public FrmTechnician()
        {
            InitializeComponent();
        }

        private void cmdUserLogin_Click(object sender, EventArgs e)
        {
            FrmLogin frmLogin = new FrmLogin();
            frmLogin.Show();
            Visible = false;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void cmdLogin_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtEmail.Text != "" && txtPassword.Text != "")
                {
                    if (Login.LoginTechnician(txtEmail.Text, password.CurrentPassword, this.technician))
                    {
                        FrmHomeTechnician frmHomeTechnician = new FrmHomeTechnician(this.technician);
                        frmHomeTechnician.Show();
                        Visible = false;
                        json.Write(lblEmail.Text);
                    }
                }
                else
                {
                    throw new Exception("Veuillez remplir les champs manquants");
                }
            }
            catch (WrongInformationException ex)
            {
                lblBadData.Text = ex.Message;
                lblBadData.Visible = true;
                txtPassword.Text = "";
            }
            catch (Exception ex)
            {
                lblBadData.Text = ex.Message;
                lblBadData.Visible = true;
                txtPassword.Text = "";
            }
        }
    }
}
