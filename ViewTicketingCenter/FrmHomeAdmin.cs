﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ModelTicketingCenter;

namespace ViewTicketingCenter
{
    public partial class FrmHomeAdmin : Form
    {
        private Technician technician;
        public FrmHomeAdmin(Technician technician)
        {
            this.technician = new Technician();
            this.technician = technician;
            InitializeComponent();
        }
        /// <summary>
        /// This method will close the application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        /// <summary>
        /// This method will minimize the application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        /// <summary>
        /// This method will restart the apllication
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdDisconnect_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }
        /// <summary>
        /// This method will just display the admin's email
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmHomeTechnician_Load(object sender, EventArgs e)
        {
            lblEmail.Text = technician.Email;
        }
    }
}
