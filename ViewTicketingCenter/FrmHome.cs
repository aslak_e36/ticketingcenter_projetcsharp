﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ModelTicketingCenter;

namespace ViewTicketingCenter
{
    public partial class FrmHome : Form
    {
        [DllImport("user32.dll", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.dll", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wwmsg, int wparam, int lparam);

        private User user;
        private UserSettings userSettings;
        private JsonReader json;
        private List<Ticket> tickets = new List<Ticket>();

        public FrmHome(User user)
        {
            this.user = user;
            InitializeComponent();
        }
        /// <summary>
        /// This methhods will load all tickets in database and display them
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmHome_Load(object sender, EventArgs e)
        {
            UCTickets oldTicket = new UCTickets();
            this.tickets = TicketTable.GetListOfTickets();
            
            lblEmail.Text = user.Email;
            UpdateHomePlace();
            pnlTicketDisplay.Controls.Add(oldTicket);

            this.Dock = DockStyle.Fill;

            foreach (var ticket in this.tickets)
            {
                UCTickets ucTicket = new UCTickets(ticket.Id, ticket.Title, ticket.Category, ticket.Description, ticket.Priority, ticket.DateStart, ticket.DateEnd, ticket.Status, ticket.Owner, ticket.Technician);
                ucTicket.Top = oldTicket.Bottom + 3;
                pnlTicketDisplay.Controls.Add(ucTicket);
                pnlTicketDisplay.VerticalScroll.Value = pnlTicketDisplay.VerticalScroll.Maximum;
                oldTicket = ucTicket;
            }
        }
        /// <summary>
        /// This methods will stop and close the application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnClose_Click(object sender, EventArgs e)
        {
            userSettings = new UserSettings(this.Location.X, this.Location.Y);
            json.Write(lblEmail.Text, userSettings);
            Application.Exit();
        }
        /// <summary>
        /// This methods will minimize the application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        /// <summary>
        /// This methods will restart the application when the user want to disconnect
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LblRestart_Click_1(object sender, EventArgs e)
        {
            Application.Restart();
        }
        /// <summary>
        /// This methods send the position of the window to model
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PnlHome_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }
        /// <summary>
        /// This methods register the place of the window
        /// </summary>
        private void UpdateHomePlace()
        {
            this.json = new JsonReader();
            int x = json.ReadUserSettings(lblEmail.Text).PositionX;
            int y = json.ReadUserSettings(lblEmail.Text).PositionY;
            userSettings = new UserSettings(x,y);
            this.Location = new Point(userSettings.PositionX, userSettings.PositionY);
        }
        /// <summary>
        /// This methods register the place of the window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmHome_FormClosed(object sender, FormClosedEventArgs e)
        {
            userSettings = new UserSettings(this.Location.X, this.Location.Y);
            json.Write(lblEmail.Text, userSettings);
        }
        /// <summary>
        /// This methods show the connected user's tickets when you click on "My Tickets" button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdTickets_Click(object sender, EventArgs e)
        {
            UCTickets oldTicket = new UCTickets();
            this.tickets = TicketTable.GetUsersTickets(user.Id);

            pnlTicketDisplay.Controls.Clear();
            pnlTicketDisplay.Controls.Add(oldTicket);

            this.Dock = DockStyle.Fill;
            foreach (var ticket in this.tickets)
            {
                UCTickets ucTicket = new UCTickets(ticket.Id, ticket.Title, ticket.Category, ticket.Description, ticket.Priority, ticket.DateStart, ticket.DateEnd, ticket.Status, ticket.Owner, ticket.Technician);
                ucTicket.Top = oldTicket.Bottom + 3;
                pnlTicketDisplay.Controls.Add(ucTicket);
                pnlTicketDisplay.VerticalScroll.Value = pnlTicketDisplay.VerticalScroll.Maximum;
                oldTicket = ucTicket;
            }
        }
        /// <summary>
        /// This methods show the form to create a new ticekt
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdAddTicekt_Click(object sender, EventArgs e)
        {
            pnlTicketDisplay.Controls.Clear();
            pnlTicketDisplay.Controls.Add(new UCAddTicket(this.user));
        }
        /// <summary>
        /// This methods shows the tickets when you click on the "Home" button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdHome_Click(object sender, EventArgs e)
        {
            UCTickets oldTicket = new UCTickets();
            this.tickets = TicketTable.GetListOfTickets();

            pnlTicketDisplay.Controls.Clear();
            pnlTicketDisplay.Controls.Add(oldTicket);

            this.Dock = DockStyle.Fill;
            foreach (var ticket in this.tickets)
            {
                UCTickets ucTicket = new UCTickets(ticket.Id, ticket.Title, ticket.Category, ticket.Description, ticket.Priority,ticket.DateStart, ticket.DateEnd, ticket.Status, ticket.Owner, ticket.Technician);
                ucTicket.Top = oldTicket.Bottom + 3;
                pnlTicketDisplay.Controls.Add(ucTicket);
                pnlTicketDisplay.VerticalScroll.Value = pnlTicketDisplay.VerticalScroll.Maximum;
                oldTicket = ucTicket;
            }
        }
    }
}