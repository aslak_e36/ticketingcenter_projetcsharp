﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ModelTicketingCenter;

namespace ViewTicketingCenter
{
    public partial class UCAddTicket : UserControl
    {
        private Ticket ticket;
        private User user;

        public UCAddTicket(User user)
        {
            InitializeComponent();
            this.user = user;
        }
        /// <summary>
        /// This methods will cancel a creation of a new ticket in database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdCancel_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Voulez-vous annuler votre création de ticket?", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr == DialogResult.Yes)
            {
                txtTitle.Text = "";
                txtDescription.Text = "";
                cmbCategory.ResetText();
                cmbPriority.Text = "";
                cmbType.Text = "";
            }
        }
        /// <summary>
        /// This method will create a new ticket in database if all conditions are OK
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdCreateTicket_Click(object sender, EventArgs e)
        {
            if (txtTitle.Text != "" && txtDescription.Text != "" && cmbType.Text != "" && cmbPriority.Text != "" && cmbCategory.Text != "")
            {
                TicketTable.CreateNewTicket(txtTitle.Text, txtDescription.Text, cmbCategory.SelectedIndex, cmbPriority.SelectedIndex, this.user.Id);
                txtTitle.Text = "";
                txtDescription.Text = "";
                cmbCategory.Text = "";
                cmbPriority.Text = "";
                cmbType.Text = "";
                DialogResult dr = MessageBox.Show("Votre ticket a bien été créé. Veuillez attendre qu'un technicien vienne vers vous.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                DialogResult dr = MessageBox.Show("Veuillez remplir les champs manquants!", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}
