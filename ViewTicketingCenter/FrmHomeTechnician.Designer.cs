﻿namespace ViewTicketingCenter
{
    partial class FrmHomeTechnician
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlHome = new System.Windows.Forms.Panel();
            this.lblAdmin = new System.Windows.Forms.Label();
            this.btnMinimize = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.lblEmail = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cmdDisconnect = new System.Windows.Forms.Button();
            this.cmdMyTickets = new System.Windows.Forms.Button();
            this.cmdTickets = new System.Windows.Forms.Button();
            this.pnlTicketDisplay = new System.Windows.Forms.Panel();
            this.pnlHome.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlHome
            // 
            this.pnlHome.BackColor = System.Drawing.Color.Salmon;
            this.pnlHome.Controls.Add(this.lblAdmin);
            this.pnlHome.Controls.Add(this.btnMinimize);
            this.pnlHome.Controls.Add(this.btnClose);
            this.pnlHome.Controls.Add(this.lblEmail);
            this.pnlHome.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHome.Location = new System.Drawing.Point(0, 0);
            this.pnlHome.Margin = new System.Windows.Forms.Padding(2);
            this.pnlHome.Name = "pnlHome";
            this.pnlHome.Size = new System.Drawing.Size(656, 40);
            this.pnlHome.TabIndex = 13;
            // 
            // lblAdmin
            // 
            this.lblAdmin.AutoSize = true;
            this.lblAdmin.Font = new System.Drawing.Font("Berlin Sans FB", 8.5F);
            this.lblAdmin.Location = new System.Drawing.Point(11, 14);
            this.lblAdmin.Name = "lblAdmin";
            this.lblAdmin.Size = new System.Drawing.Size(80, 13);
            this.lblAdmin.TabIndex = 14;
            this.lblAdmin.Text = "TECHNICIEN -";
            // 
            // btnMinimize
            // 
            this.btnMinimize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.btnMinimize.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMinimize.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinimize.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMinimize.ForeColor = System.Drawing.Color.White;
            this.btnMinimize.Location = new System.Drawing.Point(572, 1);
            this.btnMinimize.Margin = new System.Windows.Forms.Padding(2);
            this.btnMinimize.Name = "btnMinimize";
            this.btnMinimize.Size = new System.Drawing.Size(43, 38);
            this.btnMinimize.TabIndex = 11;
            this.btnMinimize.Text = "-";
            this.btnMinimize.UseVisualStyleBackColor = false;
            this.btnMinimize.Click += new System.EventHandler(this.btnMinimize_Click);
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.Red;
            this.btnClose.Location = new System.Drawing.Point(611, 1);
            this.btnClose.Margin = new System.Windows.Forms.Padding(2);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(43, 38);
            this.btnClose.TabIndex = 10;
            this.btnClose.Text = "x";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Font = new System.Drawing.Font("Berlin Sans FB", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.Location = new System.Drawing.Point(91, 14);
            this.lblEmail.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(45, 13);
            this.lblEmail.TabIndex = 13;
            this.lblEmail.Text = "lblEmail";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Salmon;
            this.panel1.Controls.Add(this.cmdDisconnect);
            this.panel1.Controls.Add(this.cmdMyTickets);
            this.panel1.Controls.Add(this.cmdTickets);
            this.panel1.Location = new System.Drawing.Point(0, 40);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(169, 413);
            this.panel1.TabIndex = 14;
            // 
            // cmdDisconnect
            // 
            this.cmdDisconnect.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.cmdDisconnect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdDisconnect.Font = new System.Drawing.Font("Berlin Sans FB", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdDisconnect.ForeColor = System.Drawing.Color.Salmon;
            this.cmdDisconnect.Location = new System.Drawing.Point(2, 367);
            this.cmdDisconnect.Margin = new System.Windows.Forms.Padding(2);
            this.cmdDisconnect.Name = "cmdDisconnect";
            this.cmdDisconnect.Size = new System.Drawing.Size(165, 44);
            this.cmdDisconnect.TabIndex = 22;
            this.cmdDisconnect.Text = "Se déconnecter";
            this.cmdDisconnect.UseVisualStyleBackColor = false;
            this.cmdDisconnect.Click += new System.EventHandler(this.cmdDisconnect_Click);
            // 
            // cmdMyTickets
            // 
            this.cmdMyTickets.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.cmdMyTickets.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdMyTickets.Font = new System.Drawing.Font("Berlin Sans FB", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdMyTickets.ForeColor = System.Drawing.Color.Salmon;
            this.cmdMyTickets.Location = new System.Drawing.Point(2, 42);
            this.cmdMyTickets.Margin = new System.Windows.Forms.Padding(2);
            this.cmdMyTickets.Name = "cmdMyTickets";
            this.cmdMyTickets.Size = new System.Drawing.Size(165, 44);
            this.cmdMyTickets.TabIndex = 19;
            this.cmdMyTickets.Text = "Mes tickets";
            this.cmdMyTickets.UseVisualStyleBackColor = false;
            this.cmdMyTickets.Click += new System.EventHandler(this.cmdMyTickets_Click);
            // 
            // cmdTickets
            // 
            this.cmdTickets.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.cmdTickets.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdTickets.Font = new System.Drawing.Font("Berlin Sans FB", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdTickets.ForeColor = System.Drawing.Color.Salmon;
            this.cmdTickets.Location = new System.Drawing.Point(2, -2);
            this.cmdTickets.Margin = new System.Windows.Forms.Padding(2);
            this.cmdTickets.Name = "cmdTickets";
            this.cmdTickets.Size = new System.Drawing.Size(165, 44);
            this.cmdTickets.TabIndex = 18;
            this.cmdTickets.Text = "Tickets";
            this.cmdTickets.UseVisualStyleBackColor = false;
            this.cmdTickets.Click += new System.EventHandler(this.cmdTickets_Click);
            // 
            // pnlTicketDisplay
            // 
            this.pnlTicketDisplay.AutoScroll = true;
            this.pnlTicketDisplay.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlTicketDisplay.Location = new System.Drawing.Point(172, 40);
            this.pnlTicketDisplay.Name = "pnlTicketDisplay";
            this.pnlTicketDisplay.Size = new System.Drawing.Size(484, 413);
            this.pnlTicketDisplay.TabIndex = 15;
            // 
            // FrmHomeTechnician
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.ClientSize = new System.Drawing.Size(656, 453);
            this.Controls.Add(this.pnlTicketDisplay);
            this.Controls.Add(this.pnlHome);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmHomeTechnician";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.FrmHomeTechnician_Load);
            this.pnlHome.ResumeLayout(false);
            this.pnlHome.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlHome;
        private System.Windows.Forms.Button btnMinimize;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblAdmin;
        private System.Windows.Forms.Button cmdDisconnect;
        private System.Windows.Forms.Button cmdMyTickets;
        private System.Windows.Forms.Button cmdTickets;
        private System.Windows.Forms.Panel pnlTicketDisplay;
    }
}