﻿namespace ViewTicketingCenter
{
    partial class FrmHome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmHome));
            this.btnClose = new System.Windows.Forms.Button();
            this.btnMinimize = new System.Windows.Forms.Button();
            this.pnlHome = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cmdHome = new System.Windows.Forms.Button();
            this.cmdAddTicekt = new System.Windows.Forms.Button();
            this.cmdTickets = new System.Windows.Forms.Button();
            this.lblRestart = new System.Windows.Forms.Label();
            this.picUser = new System.Windows.Forms.PictureBox();
            this.lblEmail = new System.Windows.Forms.Label();
            this.pnlTicketDisplay = new System.Windows.Forms.Panel();
            this.pnlHome.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picUser)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.Red;
            this.btnClose.Location = new System.Drawing.Point(616, 1);
            this.btnClose.Margin = new System.Windows.Forms.Padding(2);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(39, 46);
            this.btnClose.TabIndex = 10;
            this.btnClose.Text = "x";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // btnMinimize
            // 
            this.btnMinimize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.btnMinimize.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMinimize.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btnMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinimize.Font = new System.Drawing.Font("Berlin Sans FB", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMinimize.ForeColor = System.Drawing.Color.White;
            this.btnMinimize.Location = new System.Drawing.Point(577, 1);
            this.btnMinimize.Margin = new System.Windows.Forms.Padding(2);
            this.btnMinimize.Name = "btnMinimize";
            this.btnMinimize.Size = new System.Drawing.Size(39, 46);
            this.btnMinimize.TabIndex = 11;
            this.btnMinimize.Text = "-";
            this.btnMinimize.UseVisualStyleBackColor = false;
            this.btnMinimize.Click += new System.EventHandler(this.BtnMinimize_Click);
            // 
            // pnlHome
            // 
            this.pnlHome.BackColor = System.Drawing.Color.Salmon;
            this.pnlHome.Controls.Add(this.panel1);
            this.pnlHome.Controls.Add(this.btnMinimize);
            this.pnlHome.Controls.Add(this.btnClose);
            this.pnlHome.Controls.Add(this.lblRestart);
            this.pnlHome.Controls.Add(this.picUser);
            this.pnlHome.Controls.Add(this.lblEmail);
            this.pnlHome.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHome.Location = new System.Drawing.Point(0, 0);
            this.pnlHome.Margin = new System.Windows.Forms.Padding(2);
            this.pnlHome.Name = "pnlHome";
            this.pnlHome.Size = new System.Drawing.Size(656, 89);
            this.pnlHome.TabIndex = 12;
            this.pnlHome.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PnlHome_MouseDown);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cmdHome);
            this.panel1.Controls.Add(this.cmdAddTicekt);
            this.panel1.Controls.Add(this.cmdTickets);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 45);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(656, 44);
            this.panel1.TabIndex = 19;
            // 
            // cmdHome
            // 
            this.cmdHome.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.cmdHome.Dock = System.Windows.Forms.DockStyle.Left;
            this.cmdHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdHome.Font = new System.Drawing.Font("Berlin Sans FB", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdHome.ForeColor = System.Drawing.Color.Salmon;
            this.cmdHome.Location = new System.Drawing.Point(0, 0);
            this.cmdHome.Margin = new System.Windows.Forms.Padding(2);
            this.cmdHome.Name = "cmdHome";
            this.cmdHome.Size = new System.Drawing.Size(177, 44);
            this.cmdHome.TabIndex = 16;
            this.cmdHome.Text = "Home";
            this.cmdHome.UseVisualStyleBackColor = false;
            this.cmdHome.Click += new System.EventHandler(this.CmdHome_Click);
            // 
            // cmdAddTicekt
            // 
            this.cmdAddTicekt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.cmdAddTicekt.Dock = System.Windows.Forms.DockStyle.Right;
            this.cmdAddTicekt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAddTicekt.Font = new System.Drawing.Font("Berlin Sans FB", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAddTicekt.ForeColor = System.Drawing.Color.Salmon;
            this.cmdAddTicekt.Location = new System.Drawing.Point(479, 0);
            this.cmdAddTicekt.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAddTicekt.Name = "cmdAddTicekt";
            this.cmdAddTicekt.Size = new System.Drawing.Size(177, 44);
            this.cmdAddTicekt.TabIndex = 18;
            this.cmdAddTicekt.Text = "Créer un ticket";
            this.cmdAddTicekt.UseVisualStyleBackColor = false;
            this.cmdAddTicekt.Click += new System.EventHandler(this.CmdAddTicekt_Click);
            // 
            // cmdTickets
            // 
            this.cmdTickets.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.cmdTickets.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdTickets.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdTickets.Font = new System.Drawing.Font("Berlin Sans FB", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdTickets.ForeColor = System.Drawing.Color.Salmon;
            this.cmdTickets.Location = new System.Drawing.Point(0, 0);
            this.cmdTickets.Margin = new System.Windows.Forms.Padding(2);
            this.cmdTickets.Name = "cmdTickets";
            this.cmdTickets.Size = new System.Drawing.Size(656, 44);
            this.cmdTickets.TabIndex = 17;
            this.cmdTickets.Text = "Mes Tickets";
            this.cmdTickets.UseVisualStyleBackColor = false;
            this.cmdTickets.Click += new System.EventHandler(this.CmdTickets_Click);
            // 
            // lblRestart
            // 
            this.lblRestart.AutoSize = true;
            this.lblRestart.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblRestart.Font = new System.Drawing.Font("Berlin Sans FB", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRestart.Location = new System.Drawing.Point(42, 24);
            this.lblRestart.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblRestart.Name = "lblRestart";
            this.lblRestart.Size = new System.Drawing.Size(83, 13);
            this.lblRestart.TabIndex = 15;
            this.lblRestart.Text = "Se déconnecter";
            this.lblRestart.Click += new System.EventHandler(this.LblRestart_Click_1);
            // 
            // picUser
            // 
            this.picUser.Image = global::ViewTicketingCenter.Properties.Resources.user_male2_128;
            this.picUser.InitialImage = ((System.Drawing.Image)(resources.GetObject("picUser.InitialImage")));
            this.picUser.Location = new System.Drawing.Point(2, 2);
            this.picUser.Margin = new System.Windows.Forms.Padding(2);
            this.picUser.Name = "picUser";
            this.picUser.Size = new System.Drawing.Size(35, 37);
            this.picUser.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picUser.TabIndex = 14;
            this.picUser.TabStop = false;
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Font = new System.Drawing.Font("Berlin Sans FB", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.Location = new System.Drawing.Point(42, 6);
            this.lblEmail.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(45, 13);
            this.lblEmail.TabIndex = 13;
            this.lblEmail.Text = "lblEmail";
            // 
            // pnlTicketDisplay
            // 
            this.pnlTicketDisplay.AutoScroll = true;
            this.pnlTicketDisplay.AutoSize = true;
            this.pnlTicketDisplay.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnlTicketDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlTicketDisplay.Location = new System.Drawing.Point(0, 89);
            this.pnlTicketDisplay.Margin = new System.Windows.Forms.Padding(2);
            this.pnlTicketDisplay.Name = "pnlTicketDisplay";
            this.pnlTicketDisplay.Size = new System.Drawing.Size(656, 364);
            this.pnlTicketDisplay.TabIndex = 13;
            // 
            // FrmHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.ClientSize = new System.Drawing.Size(656, 453);
            this.ControlBox = false;
            this.Controls.Add(this.pnlTicketDisplay);
            this.Controls.Add(this.pnlHome);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrmHome";
            this.Text = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmHome_FormClosed);
            this.Load += new System.EventHandler(this.FrmHome_Load);
            this.pnlHome.ResumeLayout(false);
            this.pnlHome.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picUser)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnMinimize;
        private System.Windows.Forms.Panel pnlHome;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.PictureBox picUser;
        private System.Windows.Forms.Label lblRestart;
        private System.Windows.Forms.Button cmdHome;
        private System.Windows.Forms.Button cmdTickets;
        private System.Windows.Forms.Button cmdAddTicekt;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnlTicketDisplay;
    }
}

