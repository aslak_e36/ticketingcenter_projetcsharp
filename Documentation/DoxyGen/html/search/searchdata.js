var indexSectionsWithContent =
{
  0: "abcdefghijlmprstuvw",
  1: "adefhjlprstu",
  2: "mtv",
  3: "adfhjlprstu",
  4: "acdfgilrstuw",
  5: "p",
  6: "bmsvw",
  7: "depst",
  8: "ailrtw"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "enums",
  6: "enumvalues",
  7: "properties",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Enumerations",
  6: "Enumerator",
  7: "Properties",
  8: "Pages"
};

