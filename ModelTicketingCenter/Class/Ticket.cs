﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelTicketingCenter
{
    public class Ticket
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public string Priority { get; set; }
        public string DateStart { get; set; }
        public string DateEnd { get; set; }
        public string Status { get; set; }
        public string Owner { get; set; }
        public string Technician { get; set; }

        public Ticket() { }

        /// <summary>
        /// Constructor for a new ticket
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="category">category</param>
        /// <param name="title">title</param>
        /// <param name="description">description</param>
        /// <param name="priority">priority</param>
        /// <param name="status">status</param>
        public Ticket(  
            int id, string title, string category, 
            string description, string priority, string dateStart, string dateEnd,
            string status, string owner, string technician
        )
        {
            Id = id;
            Title = title;
            Category = category;
            Description = description;
            Priority = priority;
            DateStart = dateStart;
            DateEnd = dateEnd;
            Status = status;
            Owner = owner;
            Technician = technician;
        }
    }
}
