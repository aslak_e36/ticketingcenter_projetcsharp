﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelTicketingCenter
{
    /// <summary>
    /// Status Class
    /// </summary>
    public class Status
    {
        public int Id { get; set; }
        public string Name { get; set; }

        /// <summary>
        /// Constructs a status
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="name">name</param>
        public Status(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}
