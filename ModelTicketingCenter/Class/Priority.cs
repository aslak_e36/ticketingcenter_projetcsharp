﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelTicketingCenter
{
    /// <summary>
    /// Priority Class
    /// </summary>
    public class Priority
    {
        public int Id { get; set; }
        public string Name { get; set; }

        /// <summary>
        /// Constructs a Priority
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="name">name</param>
        public Priority(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}
