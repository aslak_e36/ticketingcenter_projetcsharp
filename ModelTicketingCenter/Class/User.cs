﻿
namespace ModelTicketingCenter
{
    /// <summary>
    /// This class will allow to create a user
    /// </summary>
    public class User
    {
        #region private attributs
        private string email;
        private int id;
        #endregion

        #region constructor
        /// <summary>
        /// It will construct a user
        /// </summary>
        /// <param name="id">User's id</param>
        /// <param name="email">User email</param>
        public User(int id, string email)
        {
            this.id = id;
            this.email = email;
        }

        public User() { }
        #endregion

        #region public method
        #endregion

        #region accessors
        public string Email { get => this.email; set => this.email = value; }
        public int Id { get => this.id; set => this.id = value; }
        #endregion
    }
}
