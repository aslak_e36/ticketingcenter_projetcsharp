﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelTicketingCenter
{
    /// <summary>
    /// Category Class
    /// </summary>
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }

        /// <summary>
        /// Constructs a Category
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="name">name</param>
        public Category(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}
