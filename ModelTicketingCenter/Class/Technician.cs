﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelTicketingCenter
{
    /// <summary>
    /// Technician Class allows us to interact with technicians
    /// </summary>
    public class Technician
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public int Type { get; set; }

        public Technician() { }

        /// <summary>
        /// Constructs a Technician
        /// </summary>
        /// <param name="id">id</param>
        /// <param name="email">email</param>
        /// <param name="type">type (0=admib, 1=technician</param>
        public Technician(int id, string email, int type)
        {
            Id = id;
            Email = email;
            Type = type;
        }
    }
}
