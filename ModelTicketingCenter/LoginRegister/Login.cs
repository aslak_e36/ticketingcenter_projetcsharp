﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ModelTicketingCenter
{
    /// <summary>
    /// this class will handle the user's Login
    /// </summary>
    public class Login
    {
        #region Public methods
        /// <summary>
        /// This method will allow our user to login
        /// </summary>
        /// <param name="email">User's email</param>
        /// <param name="password">User's password</param>
        /// <returns>True or Exception</returns>
        static public bool LoginUser(string email, string password, User user)
        {
            string checkPass = "";

            DbConnector connector = new DbConnector();

            string query = "SELECT * FROM users WHERE email = '" + email + "';";
            MySqlDataReader reader = connector.Select(query);

            while (reader.Read())
            {
                user.Id = Int32.Parse(reader["iduser"] + "");
                user.Email = reader["email"] + "";
                checkPass = reader["password"] + "";
            }
            reader.Close();
            connector.CloseConnection();

            if (user != null)
            {
                string hash = Hasher.GetHashString(password).ToLower();
                if (checkPass == hash)
                {
                    return true;
                }
                throw new WrongInformationException("Les informations saisies sont incorrectes. Veuillez revérifier");
            }
            throw new WrongInformationException("Les informations saisies sont incorrectes. Veuillez revérifier");
        }

        /// <summary>
        /// Allow technician login
        /// </summary>
        /// <param name="email">Email</param>
        /// <param name="password">Password</param>
        /// <param name="technician">Technician(obj)</param>
        /// <returns>True or Exception</returns>
        static public bool LoginTechnician(string email, string password, Technician technician)
        {
            string checkPass = "";

            DbConnector connector = new DbConnector();

            string query = "SELECT * FROM technicians WHERE email = '" + email + "';";
            MySqlDataReader reader = connector.Select(query);

            while (reader.Read())
            {
                technician.Id = Int32.Parse(reader["idtechnician"] + "");
                technician.Email = reader["email"] + "";
                technician.Type = Int32.Parse(reader["type"] + "");
                checkPass = reader["password"] + "";
            }
            reader.Close();
            connector.CloseConnection();

            if (technician != null)
            {
                string hash = Hasher.GetHashString(password).ToLower();
                if (checkPass == hash)
                {
                    return true;
                }
                throw new WrongInformationException("Les informations saisies sont incorrectes. Veuillez revérifier");
            }
            throw new WrongInformationException("Les informations saisies sont incorrectes. Veuillez revérifier");
        }
        #endregion
    }


    [Serializable]
    public class WrongInformationException : Exception
    {
        public WrongInformationException()
        {
        }

        public WrongInformationException(string message) : base(message)
        {
        }

        public WrongInformationException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected WrongInformationException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }

}
