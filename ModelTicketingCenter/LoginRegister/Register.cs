﻿using MySql.Data.MySqlClient;
using System;
using System.Runtime.Serialization;

namespace ModelTicketingCenter
{
    /// <summary>
    /// This class will allow us to Register a new user
    /// </summary>
    public class Register
    {
        #region Public methods
        /// <summary>
        /// This methods will Register a new user and if he exists it will throw an exception
        /// </summary>
        /// <param name="email">User's email</param>
        /// <param name="password">User's password</param>
        /// <returns></returns>
        public static bool RegisterUser(string email, string password, User user)
        {
            DbConnector connector = new DbConnector();
            string _email = "";

            string query = "SELECT email FROM users WHERE email = '" + email + "';";
            MySqlDataReader reader = connector.Select(query);

            while (reader.Read())
            {
                _email = reader["email"].ToString();
            }

            reader.Close();
            connector.CloseConnection();

            if (_email == "")
            {
                string hash = Hasher.GetHashString(password).ToLower();

                connector = new DbConnector();
                connector.Insert("INSERT INTO users (email,password) VALUES ('" + email + "','" + hash + "')");

                reader = connector.Select("select max(iduser) as lastid from users");

                while (reader.Read())
                {
                    user.Id = Int32.Parse(reader["lastid"] + "");
                    user.Email = email;
                }

                reader.Close();
                connector.CloseConnection();

                return true;
            }
            else
            {
                throw new ExistingEmailException("Impossible d'inscrire cette utilisateur");
            }
        }
        #endregion
    }

    [Serializable]
    public class ExistingEmailException : Exception
    {
        public ExistingEmailException()
        {
        }

        public ExistingEmailException(string message) : base(message)
        {
        }

        public ExistingEmailException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ExistingEmailException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
