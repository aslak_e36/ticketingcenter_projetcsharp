﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelTicketingCenter
{
    /// <summary>
    /// Interatcts with Db concerning Statuses
    /// </summary>
    public class StatusTable
    {
        /// <summary>
        /// Prepare query to select all statuses from Db
        /// </summary>
        /// <returns>List of Status</returns>
        public static List<Status> GetStatus()
        {
            string q = "select * from status";
            return Select(q);
        }

        /// <summary>
        /// Selects statuses from Db and creats instence of each status to insert them into a List
        /// </summary>
        /// <param name="q"></param>
        /// <returns>List of Status</returns>
        private static List<Status> Select(string q)
        {
            DbConnector db = new DbConnector();
            List<Status> statuss = new List<Status>();
            MySqlDataReader data = db.Select(q);

            while (data.Read())
            {
                int id = Int32.Parse(data["idstatus"] + "");
                string name = data["name"] + "";
                Status status = new Status(id, name);
                statuss.Add(status);
            }

            data.Close();
            db.CloseConnection();

            return statuss;
        }
    }
}
