﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelTicketingCenter
{
    /// <summary>
    /// Interact with Db concerning categories
    /// </summary>
    public class CategoriesTable
    {
        /// <summary>
        /// Prepares query to select all categories from Db
        /// </summary>
        /// <returns>List of cateory</returns>
        public static List<Category> GetCategories()
        {
            string q = "select * from categories";
            return Select(q);
        }

        /// <summary>
        /// Selects categories from Db and create inctence of Category, then add the Category to the List
        /// </summary>
        /// <param name="q">Query request</param>
        /// <returns>List of category</returns>
        private static List<Category> Select(string q)
        {
            DbConnector db = new DbConnector();
            List<Category> categories = new List<Category>();
            MySqlDataReader data = db.Select(q);

            while (data.Read())
            {
                int id = Int32.Parse(data["idcategory"] + "");
                string name = data["name"] + "";
                Category category = new Category(id, name);
                categories.Add(category);
            }

            data.Close();
            db.CloseConnection();

            return categories;
        }
    }
}
