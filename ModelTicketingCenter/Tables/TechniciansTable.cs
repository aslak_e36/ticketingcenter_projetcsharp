﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelTicketingCenter
{
    /// <summary>
    /// Interacts with Database, all that concerns Technician table
    /// </summary>
    public class TechniciansTable
    {
        /// <summary>
        /// Gets all technician from Db
        /// </summary>
        /// <returns>List of Technician</returns>
        public static List<Technician> GetTechnicians()
        {
            string q = "select * from technicians";
            return Select(q);
        }

        /// <summary>
        /// Selects technicians from Db and creats instence of Technician 
        /// </summary>
        /// <param name="q"></param>
        /// <returns></returns>
        private static List<Technician> Select(string q)
        {
            DbConnector db = new DbConnector();
            List<Technician> technicians = new List<Technician>();
            MySqlDataReader data = db.Select(q);

            while (data.Read())
            {
                int id = Int32.Parse(data["idtechnician"] + "");
                string name = data["name"] + "";
                int type = Int32.Parse(data["type"] + "");
                Technician technician = new Technician(id, name, type);
                technicians.Add(technician);
            }

            data.Close();
            db.CloseConnection();

            return technicians;
        }
    }
}
