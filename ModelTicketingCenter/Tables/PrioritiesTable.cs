﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelTicketingCenter
{
    /// <summary>
    /// Interact with Db concerning Priorities
    /// </summary>
    public class PrioritiesTable
    {
        /// <summary>
        /// Prepares query to select all priorities from Db
        /// </summary>
        /// <returns></returns>
        public static List<Priority> GetPriorites()
        {
            string q = "select * from priorities";
            return Select(q);
        }

        /// <summary>
        /// Selects priorities from Db and create inctence of Priority, then add to Priority to the List
        /// </summary>
        /// <param name="q">Query request</param>
        /// <returns>List of priority</returns>
        private static List<Priority> Select(string q)
        {
            DbConnector db = new DbConnector();
            List<Priority> priorities = new List<Priority>();
            MySqlDataReader data = db.Select(q);

            while (data.Read())
            {
                int id = Int32.Parse(data["idpriority"] + "");
                string name = data["name"] + "";
                Priority priority = new Priority(id, name);
                priorities.Add(priority);

            }

            data.Close();
            db.CloseConnection();

            return priorities;
        }
    }
}
