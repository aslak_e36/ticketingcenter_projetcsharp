﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelTicketingCenter
{
    /// <summary>
    /// This class will interact with database for requests concerning tickets
    /// </summary>
    public class TicketTable
    {
        /// <summary>
        /// This method will select ticekts that a technician owns
        /// </summary>
        /// <param name="techId">Technician's uniq ID</param>
        /// <returns>Will return a list of Ticket (the object)</returns>
        public static List<Ticket> GetTechTickets(int techId)
        {
            string q = "SELECT tickets.idticket, tickets.title, " +
                "categories.name AS category, tickets.description, " +
                "priorities.name AS priority, tickets.datestart, tickets.dateend, " +
                "status.name AS status, users.email AS owner, technicians.email AS technician " +
                "FROM tickets " +
                "LEFT JOIN categories ON categories_idcategory = categories.idcategory " +
                "LEFT JOIN priorities ON priorities_idpriority = priorities.idpriority " +
                "LEFT JOIN status ON status_idstatus = status.idstatus " +
                "LEFT JOIN users ON users_iduser = users.iduser " +
                "LEFT JOIN technicians ON technicians_idtechnician = technicians.idtechnician " +
                "WHERE technicians_idtechnician = " + techId + " " +
                "ORDER BY datestart desc";

            return Select(q);
        }

        /// <summary>
        /// Select user's tickets
        /// </summary>
        /// <param name="userId">User's ID</param>
        /// <returns>A list of Ticket</returns>
        public static List<Ticket> GetUsersTickets(int userId)
        {
            string q = "SELECT tickets.idticket, tickets.title, " +
                "categories.name AS category, tickets.description, " +
                "priorities.name AS priority, tickets.datestart, tickets.dateend, " +
                "status.name AS status, users.email AS owner, technicians.email AS technician " +
                "FROM tickets " +
                "LEFT JOIN categories ON categories_idcategory = categories.idcategory " +
                "LEFT JOIN priorities ON priorities_idpriority = priorities.idpriority " +
                "LEFT JOIN status ON status_idstatus = status.idstatus " +
                "LEFT JOIN users ON users_iduser = users.iduser " +
                "LEFT JOIN technicians ON technicians_idtechnician = technicians.idtechnician " +
                "WHERE users_iduser = " + userId + " " +
                "ORDER BY datestart desc";

            return Select(q);
        }

        /// <summary>
        /// Get all tickets in Db
        /// </summary>
        /// <returns>List of Ticket</returns>
        public static List<Ticket> GetListOfTickets()
        {
            string q = "SELECT tickets.idticket, tickets.title, " +
                "categories.name AS category, tickets.description, " +
                "priorities.name AS priority, tickets.datestart, tickets.dateend, " +
                "status.name AS status, users.email AS owner, technicians.email AS technician " +
                "FROM tickets " +
                "LEFT JOIN categories ON categories_idcategory = categories.idcategory " +
                "LEFT JOIN priorities ON priorities_idpriority = priorities.idpriority " +
                "LEFT JOIN status ON status_idstatus = status.idstatus " +
                "LEFT JOIN users ON users_iduser = users.iduser " +
                "LEFT JOIN technicians ON technicians_idtechnician = technicians.idtechnician " +
                "ORDER BY datestart asc";

            return Select(q);
        }

        /// <summary>
        /// Inserts a new ticket in Db
        /// </summary>
        /// <param name="title">Title</param>
        /// <param name="description">Description</param>
        /// <param name="category">Category</param>
        /// <param name="priority">Priority</param>
        /// <param name="userid">User ID</param>
        public static void CreateNewTicket(string title, string description, int category, int priority, int userid)
        {
            DbConnector db = new DbConnector();
            string date = DateTime.Now.ToString("yyyy/M/d");
            int status = 0;
            string q = "INSERT INTO `tickets` (`title`, `description`, `datestart`, `categories_idcategory`, `priorities_idpriority`, `status_idstatus`, `users_iduser`) " +
                       "VALUE ('" + title + "', '" + description + "', '" + date + "', " + category + ", " + priority + ", " + status + ", " + userid + ")";
            db.Insert(q);
        }

        /// <summary>
        /// Creates instence of Ticket foreach ticket revieved in request (q), and adds the tiket to the List<ticket>()
        /// </summary>
        /// <param name="q">Query request</param>
        /// <returns>List of Ticket</returns>
        private static List<Ticket> Select(string q)
        {
            DbConnector connector = new DbConnector();
            List<Ticket> tickets = new List<Ticket>();
            MySqlDataReader data = connector.Select(q);

            while (data.Read())
            {
                int id = Int32.Parse(data["idticket"] + "");
                string title = data["title"] + "";
                string category = data["category"] + "";
                string description = data["description"] + "";
                string priority = data["priority"] + "";
                string dateStart = data["datestart"] + "";
                string dateEnd = data["dateend"] + "";
                string status = data["status"] + "";
                string owner = data["owner"] + "";
                string technician = data["technician"] + "";
                Ticket ticket = new Ticket(id, title, category, description, priority, dateStart, dateEnd, status, owner, technician);
                tickets.Add(ticket);
            }
            data.Close();
            connector.CloseConnection();

            return tickets;
        }

        /// <summary>
        /// Allows a technician to take a ticket to take care of the problem
        /// </summary>
        /// <param name="technician">The object Technician</param>
        /// <param name="ticketId">Ticket ID</param>
        public static void TakeATicket(Technician technician, int ticketId)
        {
            DbConnector connector = new DbConnector();
            string q = "update tickets set technicians_idtechnician = " + technician.Id + " where idticket = " + ticketId + " ;";
            connector.Update(q);
        }
    }
}
