﻿namespace ModelTicketingCenter
{
    /// <summary>
    /// This class is for the application itself's setting like String connection to Database
    /// </summary>
    public class AppSettings
    {
        #region Private attributs
        private string dbStringConnection;
        #endregion

        #region Accessors
        /// <summary>
        /// We'll get and set the string connection to the database
        /// </summary>
        public string DbStringConnection { get => dbStringConnection; set => this.dbStringConnection = value; }
        #endregion
    }
}