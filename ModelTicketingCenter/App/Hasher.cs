﻿using System.Security.Cryptography;
using System.Text;

namespace ModelTicketingCenter
{
    /// <summary>
    /// This class will hash the user's password
    /// </summary>
    public class Hasher
    {
        #region Public methods
        /// <summary>
        /// This will encode the password
        /// </summary>
        /// <param name="inputString"></param>
        /// <returns></returns>
        public static byte[] GetHash(string inputString)
        {
            HashAlgorithm algorithm = SHA256.Create();
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        public static string GetHashString(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString))
            {
                sb.Append(b.ToString("X2"));
            }

            return sb.ToString();
        }
        #endregion
    }
}