﻿using Newtonsoft.Json;
using System.IO;
using System.Windows.Forms;

namespace ModelTicketingCenter
{
    /// <summary>
    /// This class will allow us to read and write into JSON files
    /// </summary>
    public class JsonReader
    {
        #region Constants
        const string path = @"\JSON\";
        #endregion

        public JsonReader() { }

        #region Public methotds
        /// <summary>
        /// Here, we'll read settings from Settings JSON file and return an object (AppSettings)
        /// </summary>
        /// <returns>stringFromJson => AppSettings</returns>
        public static AppSettings ReadDbConnectionSettings()
        {
            string jsonFromFile;
            using (var reader = new StreamReader("settings.json"))
            {
                jsonFromFile = reader.ReadToEnd();
            }
            AppSettings settingsFromJson = JsonConvert.DeserializeObject<AppSettings>(jsonFromFile);
            return settingsFromJson;
        }

        /// <summary>
        /// This method will read the user settings from JSON file and returns de settings
        /// </summary>
        /// <param name="email">User's email</param>
        /// <returns>User's settings</returns>
        public UserSettings ReadUserSettings(string email)
        {
            string jsonFromFile;

            if (!File.Exists(email + ".settings.json"))
            {
                FileStream fs = File.Create(email + ".settings.json");
                fs.Close();
                using (StreamWriter file = new StreamWriter(email + ".settings.json"))
                {
                    file.WriteLine("{ \"PositionX\" : 0, \"PositionY\" : 0}");
                    file.Close();
                }
            }

            using (var reader = new StreamReader(email + ".settings.json"))
            {
                jsonFromFile = reader.ReadToEnd();
            }
            UserSettings settingsFromJson = JsonConvert.DeserializeObject<UserSettings>(jsonFromFile);
            return settingsFromJson;
        }

        /// <summary>
        /// This methods will allow us to write into JSON file the user settings
        /// </summary>
        /// <param name="email">Which user is saving the settings</param>
        /// <param name="settings">the setting itselfs</param>
        public void Write(string email, UserSettings settings = null)
        {
            if (settings != null)
            {
                string output = JsonConvert.SerializeObject(settings);

                if (!File.Exists(email + ".settings.json"))
                {
                    FileStream fs = File.Create(email + ".settings.json");
                }

                using (StreamWriter writer = new StreamWriter(email + ".settings.json"))
                {
                    writer.WriteLine(output);
                }
            }else
            {
                FileStream fs = File.Create(email + ".settings.json");
                fs.Close();
            }
        }
        #endregion
    }
}
