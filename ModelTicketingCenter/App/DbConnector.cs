﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace ModelTicketingCenter
{
    /// <summary>
    /// This class will allow us to connect to the DataBase
    /// </summary> 
    public class DbConnector
    {
        #region private attributes
        private MySqlConnection connection;
        private string connectionString;
        #endregion

        #region Constructor
        /// <summary>
        /// We consctruct our objet for instance
        /// </summary>
        public DbConnector()
        {
            Initialize();
        }
        #endregion

        #region Private methods
        /// <summary>
        /// Here we initialize a connection with Database
        /// </summary>
        private void Initialize()
        {
            AppSettings appSettings = new AppSettings();
            this.connectionString = JsonReader.ReadDbConnectionSettings().DbStringConnection;
            //this.connectionString = "Server = ticketcenter.mysql.database.azure.com; Port = 3306; Database = ticketcenter; UID = ticketcenter@ticketcenter; PASSWORD = 1a2s3D_azure;";
            connection = new MySqlConnection(connectionString);
        }
        
        /// <summary>
        /// This will open a connection to the DB
        /// </summary>
        /// <returns>True or False</returns>
        private bool OpenConnection()
        {
            try
            {
                connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                switch (ex.Number)
                {
                    case 0:
                        MessageBox.Show("Cannot connect to server. Contact admin");
                        break;
                    case 1045:
                        MessageBox.Show("Wrong user or password");
                        break;
                }
                return false;
            }
        }
        #endregion

        #region Public methods
        /// <summary>
        /// This will close the connection
        /// </summary>
        /// <returns>True or false</returns>
        public bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// This method will allow us to read into database
        /// </summary>
        /// <param name="q">The query for the select</param>
        /// <returns>It returns an object MySqlDataRealder</returns>
        public MySqlDataReader Select(string q)
        {
            MySqlDataReader data = null;

            if (OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(q, connection);
                data = cmd.ExecuteReader();

                return data;
            }
            else
            {
                return data;
            }
        }

        /// <summary>
        /// This will allow us to insert something
        /// </summary>
        /// <param name="q"></param>
        public void Insert(string q)
        {
            if (OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(q, connection);
                cmd.ExecuteNonQuery();
                CloseConnection();
            }
        }

        /// <summary>
        /// This is exactly the same as Insert() method but for better understanding we named them after what we needed to do
        /// </summary>
        /// <param name="q">SQL query</param>
        public void Update(string q)
        {
            if (OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(q, this.connection);
                cmd.ExecuteNonQuery();
                CloseConnection();
            }
        }

        /// <summary>
        /// Same as Insert() and Update() methods but fo deleting
        /// </summary>
        /// <param name="q">SQL query</param>
        public void Delete(string q)
        {
            if (OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(q, connection);
                cmd.ExecuteNonQuery();
                CloseConnection();
            }
        }

        /// In fact you can use any of these three last methods to Insert, Update or Delete, they will act the same, but to understand what we are doing, we separeted them
        #endregion
    }
}

