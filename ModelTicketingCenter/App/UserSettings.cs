﻿namespace ModelTicketingCenter
{
    /// <summary>
    /// This class will allow us to set user settings for the windows
    /// </summary>
    public class UserSettings
    {
        #region Private attributes
        private int positionX;
        private int positionY;
        #endregion

        #region constructor
        public UserSettings(int positionX, int positionY)
        {
            PositionX = positionX;
            PositionY = positionY;
        }
        #endregion

        #region Accessors
        public int PositionX { get; set; }
        public int PositionY { get; set; }
        #endregion
    }
}
