#  Ticketing Center

## Installation de l'application

Lancez l'éxecutalbe ***TicketCenter.msi*** et procéder à une installation classique

![Installation](Documentation/img/install000.png)

![Installation](Documentation/img/install001.png) Choisissez le chemin d'installation.

![Installation](Documentation/img/install002.png)

![Installation](Documentation/img/install003.png)

## Mise en place de la base de donnée

Lancer le script qui se trouve ici -> [***/Database/DataBase.sql***](https://bitbucket.org/aslak_e36/ticketingcenter_projetcsharp/src/master/Database/DataBase.sql)

![Db](Documentation/img/db001.png)

Rendez-vous dans le dossier d'installation de l'application puis ouvrez le fichier ***settings.json*** afin de faire les modifications nécessaire pour vous connecter a votre base de données locale.

![Db](Documentation/img/db000.png)

## Utilisation de l'application

Afin que l'applciation fonctionne correctement, il faut se rendre à l'emplacement de l'installation et lancer l'application avec les privilèges "Administrateur". Autrement, lors de la sauveguarde du fichier "JSON" de l'utilisateur, l'application ne sera pas capable d'enregistrer le fichier car elle n'aura pas les droits nécaissaire et cela vous créera une erreur. (Test fais après installation de l'application sur disque C:\ , ***si l'application est installé sur un autre disque cela n'arrivera pas***)




